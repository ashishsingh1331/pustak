/**
 * Created by ashish on 7/11/16.
 */

var mongoose = require('mongoose');

var notificationSchema =  mongoose.Schema({
    toUser: {type: String, ref: 'User', index: true},
    fromUser: {type: String, ref: 'User'},
    bookId: {type: String, ref: 'Book'},
    notText: {type: String, required: true},
    ts: {type: Date, default: Date.now},
    isRead: {type: Boolean, default: false},
    verb:{type: String, required: true},
    messageId: {type: String, ref: 'Message'},
});

module.exports = mongoose.model('Notification', notificationSchema);


