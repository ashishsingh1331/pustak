var mongoose = require('mongoose');

var taxonomySchema = mongoose.Schema({
        taxonomy_title:String,
        taxonomy_terms:[String]
});

module.exports = mongoose.model('Taxonomy', taxonomySchema);


