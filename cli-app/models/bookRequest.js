/**
 * Created by ashish on 6/10/16.
 */
var mongoose = require('mongoose');

var bookRequestSchema = mongoose.Schema({
    title:{type:String,required:true},
    category:String,
    author:[String],
    created_by:{type:Object, ref:'User'},
    uploaded:{ type: Date, default: Date.now },
    completed:{ type: Boolean, default: false },
});

module.exports = mongoose.model('bookRequest', bookRequestSchema);