var mongoose = require('mongoose');

var bookSchema = mongoose.Schema({
    title:{type:String,required:true},
    category:{type:String,required:true},
    description:{type:String,required:true},
    author:[String],
    likedBy:{ type: [String], default: [] },
    uploaded:{ type: Date, default: Date.now },
    created_by:{type:Object, ref:'User'},
    contact_number:{type:String,required:true},
    img_info:Object,
    price:{type:Number,required:true},
    status:{ type: String, default: 'PENDING' },
});

module.exports = mongoose.model('Book', bookSchema);