var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');


var userSchema = mongoose.Schema({
    local:{
        email:{type:String,required:true},
        password:{type:String,required:true},
        full_name:{type:String,required:true},
        role:{type:String,required:true},
        contact_number:{type:String,required:true},
        //stream:{type:String,required:true},
        location:{type:String,required:true},
        latlng:{type:Object,required:true},
        active:{type:Number,required:true},
        subscribed_streams:{ type: [String], default: [] },
        created:{ type: Date, default: Date.now },
        liked_books:{ type: [String], default: [] },
    }
});

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};
module.exports = mongoose.model('User', userSchema);