var mongoose = require('mongoose');

var OTTSchema = mongoose.Schema({
    oneTimeToken:{type:String,required:true},
    userId:{type:Object, ref:'User'},
    status:{type:Number,default :0},
});

module.exports = mongoose.model('OTT', OTTSchema);
