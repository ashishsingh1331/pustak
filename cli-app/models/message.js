var mongoose = require('mongoose'),Schema = mongoose.Schema;

var msgSchema = mongoose.Schema({
    toUser: {type: String, ref: 'User',index: true},
    fromUser: {type: String, ref: 'User'},
    message: {type: String},
    ts: {type: Date, default: Date.now},
    isRead: {type: Boolean, default: false,index: true},
});

module.exports = mongoose.model('Message', msgSchema);


