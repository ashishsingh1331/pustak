var express = require('express');
var router = express.Router();
var Taxonomy = require('../models/taxonomy');
var Book = require('../models/book');
var fs = require('fs');
var multer = require('multer');


/* GET home page. */
router.get('*',isAdminLoggedIn,isUserAuthrised, function (req, res, next) {
    return next();
});

router.get('/taxonomy', function (req, res, next) {
    Taxonomy.find({}, function (err, taxonomies) {

        if (err)
            return next(err);

        res.render('admin_taxonomy', {taxonomies: taxonomies});
    });
});
router.get('/taxonomy/create', function (req, res, next) {

    res.render('admin_taxonomy_create');

});

router.get('/taxonomy/:tid/term/add', function (req, res, next) {

    Taxonomy.find({_id: req.params.tid}, function (err, taxonomy) {
        if (err) {
            return next(new Error('Wrong term id'));
        } else{
            res.render('admin_taxonomy_term_add', {title: taxonomy[0].taxonomy_title,id:req.params.tid,terms:taxonomy[0].taxonomy_terms});
        }


    });
});

router.post('/taxonomy/create', function (req, res, next) {

    var tax = new Taxonomy();      // create a new instance of the Taxonomy model
    tax.taxonomy_title = req.body.taxonomy_title;  // set the Taxonomys name (comes from the request)

    // save the Taxonomy and check for errors
    tax.save(function (err) {
        if (err)
            return next(err);


        res.redirect('/admin/taxonomy');

    });
});

router.post('/taxonomy/term/add', function (req, res, next) {

    Taxonomy.find({_id: req.body.documentId}, function (err,taxonomy) {
        if (err)
           return next(err);

        taxonomy[0].taxonomy_terms.push(req.body.term);

        taxonomy[0].save(function (err) {
            if (err)
             return next(err);

            var uScoredTerm =  req.body.term.split(' ').join('_')
            mailgun.lists().create({
                address: uScoredTerm.toLowerCase() +'@'+ domain,
                description:'no description',
                name:uScoredTerm.toLowerCase()
            }, function (err, body) {
                res.redirect('/admin/taxonomy/'+req.body.documentId+'/term/add');
            });


        });
    });

    // save the Taxonomy and check for errors

});

router.get('/book', function (req, res, next) {
    Taxonomy.find({}, function (err, taxonomies) {

        if (err)
            return next(err);

        res.render('admin_book');
    });
});

router.get('/book/create', function (req, res, next) {

    Taxonomy.findOne({taxonomy_title:'Stream'},function(err,terms){
        res.render('admin_book_create',{terms:terms.taxonomy_terms});
    });


});

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __dirname + '/../public/images/books')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() +'.jpg')
    }
})

var upload = multer({ storage: storage })


router.post('/book/create',upload.single('image'), function (request, response, next) {
    console.log(request.body);
    var book= new Book();      // create a new instance of the Taxonomy model
    book.title = request.body.title;  // set the Taxonomys name (comes from the request)
    book.catetory = request.body.catetory;
    book.description = request.body.description;
    book.author = request.body.author;
    book.contact_number = request.body.contact_number;
    book.created_by = request.user ;
    book.category = request.body.category ;
    book.img_info = request.file;

    book.price = request.body.price;
    // save the Taxonomy and check for errors
    book.save(function (err) {
        if (err)
            return next(err);


        return response.redirect('/admin/book/create');
    });
});

function isAdminLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}
function isUserAuthrised(req, res, next) {
    if (req.isAuthenticated() && req.user._doc.local.role == 'admin')
        return next();
    else
        return next(new Error('Access denied'));


}


module.exports = router;
