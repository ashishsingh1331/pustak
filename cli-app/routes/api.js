var express = require('express');
var router = express.Router();
var Book = require('../models/book');
var Taxonomy = require('../models/taxonomy');
var BookRequest = require('../models/bookRequest');
var multer = require('multer');
var passport = require('passport');
var OTT = require('../models/oneTimeToken');
var Notification = require('../models/notification');
var Message = require('../models/message');
var bunyan = require('bunyan');
var log = bunyan.createLogger({name: "api.js"});
var Q = require('q');

var common = require('../common');

var User = require('../models/user');

var gun = require('../mailGunApi');
//TODO-bob convert image into smaller size

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __dirname + '/../public/images/books')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.jpg')
    }
})


//
//router.get('*',isLoggedIn, function (req, res, next) {
//    return next();
//});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    return res.status(403).json({api: 'access denied'});
}
router.get('/isLoggedIn', function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()){
        return res.status(200).json({api: req.user});
    }
    return res.status(403).json({api: 'access denied'});
});


router.post('/fetchBooks', function (req, res, next) {

    var skip = (req.body.paginationOptions.pageNumber - 1) * req.body.paginationOptions.pageSize;

    var sort = {};
    if (req.body.paginationOptions.sort != null) {
        console.log(req.body.paginationOptions.name)
        var key = req.body.paginationOptions.name;
        var value = req.body.paginationOptions.sort == 'asc' ? 1 : -1;
        sort[req.body.paginationOptions.name] = value;
    } else {
        sort = {};
    }

    if (typeof req.body.query.title != 'undefined') {
        req.body.query = {
            $or: [{
                title: new RegExp(req.body.query.title)
            }, {
                author: new RegExp(req.body.query.title)
            }]
        }
    }


    var totalCount;

    Book.count({}, function (err, count) {
        totalCount = count;
    })

    Book.find(req.body.query, {'created_by.local': 0}, {
        'sort': sort,
        skip: skip,
        limit: req.body.paginationOptions.pageSize
    }, function (err, books) {
        return res.json({books: books, total: totalCount});
    });

})

router.get('/search', function (req, res, next) {
    var title = {}, author = {}, query = [];
    //param.title = '/.*'+ req.query.title+'.*/';
    //param.author = '/.*'+ req.query.title+'.*/';

    var query = [{
        title: new RegExp(req.query.title)
    }, {
        author: new RegExp(req.query.title)
    }]

    common.findSimilarBooks(query).exec(function (err, books) {
        if (err)
            return console.log(err);
        return res.json({books: books});
    });
});


router.get('/verifyOTT', function (req, res, next) {

    OTT.findOne(req.query, function (err, obj) {
        if (err || obj == null) {
            return res.status(400).json({msg: 'Not a valid one time url'});
        }

        if (obj.status == 1) {
            return res.json({msg: 'Url already validated'});
        }

        if (obj.status == 0) {
            OTT.update({_id: obj._id}, {status: 1}, function (err, numAffected) {
                if (err) {
                    return res.status(400).json({msg: 'Error while activating account'});
                }


                User.findById(obj.userId, function (err, user) {
                    if (err || user == null) {
                        return res.status(400).json({msg: 'unable to find user'});
                    }
                    user.local.active = 1;


                    user.save(function (err) {
                        if (err)
                            return next(err);

                        return res.json({msg: 'Account Activated'});

                    });


                });


            });
        }


    });
});
var upload = multer({storage: storage})

router.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/profile',
    failureRedirect: '/login',
    failureFlash: true,
}));

router.post('/book/create', function (request, response, next) {

    var book = new Book();      // create a new instance of the Taxonomy model
    book.title = request.body.title;  // set the Taxonomys name (comes from the request)
    book.catetory = request.body.catetory;
    book.description = request.body.description;
    book.author = request.body.author;
    book.contact_number = request.user.local.contact_number;
    book.created_by = request.user;
    book.category = request.body.category;
    book.img_info = request.body.img_info;
    book.price = request.body.price;
    // save the Taxonomy and check for errors
    book.save(function (err) {
        if (err)
            return response.status(400).json({msg: err});


        return response.status(200).json({msg: "Book posted successfully"});
    });
});

router.post('/book/request', function (request, response, next) {

    var bookRequest = new BookRequest();      // create a new instance of the Taxonomy model
    bookRequest.title = request.body.title;  // set the Taxonomys name (comes from the request)
    bookRequest.author = request.body.author;
    bookRequest.created_by = request.user;
    bookRequest.category = request.body.category;
    // save the Taxonomy and check for errors
    bookRequest.save(function (err) {
        if (err)
            return next(err);

        //var io = request.io;
        //io.sockets.in('padhkbecho@gmail.com').emit('new_msg', {msg: 'hello' + Date.now()});
        //io.emit('book request', {
        //   _id:bookRequest._id,
        //   bookTitle: request.body.title,
        //   bookStream: request.body.category,
        //   author: request.body.author,
        //   fullName:request.user._doc.local.full_name,
        //    ts:Math.floor(Date.now() / 1000)
        //});

        return response.status(200).json({msg: "bookRequest posted successfully. You will be notified when it gets uploaded"});
    });
});

router.post('/upload/image', upload.single('image'), function (request, response, next) {

    return response.json(request.file);
});


router.post('/subscribe/stream', isLoggedIn, function (request, response, next) {

    if (typeof request.user.subscribed_streams == 'undefined') {
        //  request.user.subscribed_streams = [];
    }
    //
    User.findById(request.user._id, function (err, user) {
        if (err || user == null) {
            return res.status(400).json({msg: 'unable to subscribe'});
        }
        user.local.subscribed_streams.push(request.body.term);


        user.save(function (err) {
            if (err)
                return next(err);

            var list = mailgun.lists(request.body.term.toLowerCase() + '@' + domain);
            list.info(function (err, data) {
                // `data` is mailing list info
            });

            var bob = {
                subscribed: true,
                address: request.user.local.email,
                name: request.user.local.full_name,
            };


            list.members().create(bob, function (err, data) {
                // `data` is the member details
                // console.log(data);

            });
            return response.json({msg: 'Subscribed'});

        });


    });


});

router.post('/unSubscribe/stream', isLoggedIn, function (request, response, next) {


    var subscribedStreams = request.user.local.subscribed_streams;
    var index = subscribedStreams.indexOf(request.body.term);
    subscribedStreams.splice(index, 1);

    User.findById(request.user._id, function (err, p) {
        if (!p)
            return response.status(400).json({msg: 'unable to subscribe'});
        else {
            // do your updates here
            p.local.subscribed_streams = subscribedStreams;

            p.save(function (err) {
                if (err)
                    response.status(400).json({msg: 'error in unsubscription'})
                else {
                    var list = mailgun.lists(request.body.term.toLowerCase() + '@' + domain);
                    list.info(function (err, data) {
                        // `data` is mailing list info
                        //console.log(data);
                    });


                    list.members(request.user.local.email).delete(function (err, body) {


                    });

                    return response.json({msg: 'Unsubscribed'});

                }
            });
        }
    });


});


router.get('/fetchStreams', function (req, res, next) {
    Taxonomy.findOne({taxonomy_title: 'Stream'}, function (err, terms) {
        //  console.log(terms);
        return res.json({terms: terms.taxonomy_terms});
    });
});

router.get('/myBooks', isLoggedIn, function (req, res, next) {
    Book.find(req.query, {created_by: 0}, function (err, books) {
        return res.json({books: books});
    });
});

router.get('/adminFetchStreams', isLoggedIn, function (req, res, next) {
    Taxonomy.findOne({taxonomy_title: 'Stream'}, function (err, terms) {
        return res.json({terms: terms.taxonomy_terms});
    });
});

router.post('/shortListBook', isLoggedIn, isAlreadyLiked, function (request, response, next) {

    var notification = new Notification();      // create a new instance of the Taxonomy model
    notification.toUser = request.body.toUser;  // set the Taxonomys name (comes from the request)
    notification.fromUser = request.user._id;
    notification.verb = 'LIKE';
    notification.bookId = request.body.bookId;
    notification.notText = 'Book you uploaded has been shortlisted by ' + request.user.local.full_name;

    // save the Taxonomy and check for errors
    notification.save(function (err) {
        if (err)
            return response.status(400).json({msg: err});


        User.findById(request.body.toUser, function (err, p) {
            if (!p)
                return response.status(400).json({msg: 'unable to find user'});
            else {
                io.sockets.in(p._id).emit('bookShortListed', {msg: notification.notText});


                if (common.addLikedBooksInUserObject(request.user._id, request.body.bookId)) {
                    log.info('book added in user object')
                }
                common.updateBookLikes(request.user._id, request.body.bookId).exec(function (err, book) {

                    if (err || book == null) {
                        log.warn(err);
                        return false;
                    }

                    book.likedBy.push(request.user._id);
                    book.save(function (err, p) {
                        if (err) {
                            log.warn(err);
                            return false;
                        }

                        common.getUnreadNotificationCount(request.body.toUser).then(function(count){
                            io.sockets.in(request.body.toUser).emit('unreadNotification', {count: count});
                        })

                        return response.status(200).json({msg: 'Liked', book: book});

                    });

                });
            }


        });


    });

});
router.post('/message', isLoggedIn,isUserExist, function (request, response, next) {

    var promise = common.saveMessage(request);

    promise.then(function (Message) {

        return common.addNotification(request, Message).then(function (notification) {

            return common.findUserById(notification.toUser).then(function (user) {
                io.sockets.in(notification.toUser).emit('message_received', {msg: notification.notText});
                common.getUnreadNotificationCount(notification.toUser).then(function(count){
                    io.sockets.in(notification.toUser).emit('unreadNotification', {count: count});
                })
                return response.status(200).json({msg: "Message Sent", notification: notification});
            })
        }).catch(function (error) {
            return response.status(400).json({msg: "Message error", notification: error});
        }).done();

    })

});

router.post('/getNotifications', isLoggedIn, function (request, response, next) {

    var promise = common.getNotificaiton(request.user._id);

    promise.then(function (notifications) {
        return response.status(200).json({ notification: notifications});
    })

});


router.post('/notificationDetail', isLoggedIn, function (request, response, next) {

    var promise = common.getNoticationObj(request.body.notificationId);

    promise.then(function (notification) {
        return common.getMessageObj(request.body.messageId).then(function(message){
         return   common.getBookObj(request.body.bookId).then(function(book){
                return response.status(200).json({ message: message,book:book,notification:notification});
            })
        })
    })

});

router.post('/getUnreadNotificationCount', isLoggedIn, function (request, response, next) {

    var promise = common.getUnreadNotificationCount(request.user._id);

    promise.then(function (count) {
        return response.status(200).json({ count: count});
    })

});

function isAlreadyLiked(request, response, next) {
    common.isAlreadyLiked(request.user._id, request.body.bookId).exec(function (err, book) {

        if (err) {
            log.warn('error in fetch book to update like')
        }

        if (book.likedBy.indexOf(request.user._id) != -1) {
            return response.status(400).json({msg: "Already Liked"});
        } else {
            return next();
        }
    });
}
function isUserExist(request, response, next) {
    var promise = common.findUserById(request.body.toUser);

    promise.then(function (user) {
    return next();
    }).catch(function (error) {
        return response.status(400).json({msg: "User does not exist"});
    })
}

//Admin APIS


router.post('/fetchTotalPending', isAdmin, function (req, res, next) {

    Book.count(req.body.query, function (err, count) {
        return res.json({total: count});
    })

});

router.post('/taxonomy/term/add', function (req, res, next) {

    Taxonomy.find({taxonomy_title: req.body.taxonomy_title}, function (err, taxonomy) {
        if (err)
            return res.status(400).json({msg: err});

        taxonomy[0].taxonomy_terms.push(req.body.term);

        taxonomy[0].save(function (err) {
            if (err)
                return res.status(400).json({msg: err});

            var uScoredTerm = req.body.term.split(' ').join('_')
            mailgun.lists().create({
                address: uScoredTerm.toLowerCase() + '@' + domain,
                description: 'no description',
                name: uScoredTerm.toLowerCase()
            }, function (err, body) {
                if (err) {
                    return res.status(400).json({msg: err});
                }
                return res.status(200).json({msg: 'Term added'});
            });


        });
    });

    // save the Taxonomy and check for errors

});

router.post('/adminFetchTaxonomies', isAdmin, function (req, res, next) {

    Taxonomy.find({}, function (err, taxonomies) {
        return res.json({taxonomies: taxonomies});
    });

});
router.post('/fetchTotalPendingUsers', isAdmin, function (req, res, next) {

    User.count(req.body.query, function (err, count) {
        return res.json({total: count});
    })

});

router.post('/approveBook', isAdmin, function (request, response, next) {

    Book.findById(request.body._id, function (err, p) {
        if (!p)
            return response.status(400).json({msg: 'unable to approve'});
        else {
            // do your updates here
            p.status = 'APPROVED';

            p.save(function (err) {
                if (err)
                    response.status(400).json({msg: 'unable to approve'})
                else {

                    return response.json({msg: 'Approved'});

                }
            });
        }
    });

});
router.post('/deleteBook', isAdmin, function (request, response, next) {

    Book.findById(request.body._id, function (err, p) {
        if (!p)
            return response.status(400).json({msg: 'unable to approve'});
        else {
            // do your updates here
            p.status = 'APPROVED';

            p.remove(function (err) {
                if (err)
                    response.status(400).json({msg: 'unable to delete'})
                else {

                    return response.json({msg: 'Deleted'});

                }
            });
        }
    });

});
router.post('/approveUser', isAdmin, function (request, response, next) {

    User.findById(request.body._id, function (err, p) {
        if (!p)
            return response.status(400).json({msg: 'unable to approve'});
        else {
            // do your updates here
            p.local.active = 1;

            p.save(function (err) {
                if (err)
                    response.status(400).json({msg: 'unable to approve'})
                else {

                    return response.json({msg: 'Approved'});

                }
            });
        }
    });

});
router.post('/deleteUser', isAdmin, function (request, response, next) {

    User.findById(request.body._id, function (err, p) {
        if (!p)
            return response.status(400).json({msg: 'unable to approve'});
        else {


            p.remove(function (err) {
                if (err)
                    response.status(400).json({msg: 'unable to delete'})
                else {

                    return response.json({msg: 'Deleted'});

                }
            });
        }
    });

});

router.post('/fetchUsers', isAdmin, function (req, res, next) {

    var skip = (req.body.paginationOptions.pageNumber - 1) * req.body.paginationOptions.pageSize;

    var sort = {};
    if (req.body.paginationOptions.sort != null) {
        console.log(req.body.paginationOptions.name)
        var key = req.body.paginationOptions.name;
        var value = req.body.paginationOptions.sort == 'asc' ? 1 : -1;
        sort[req.body.paginationOptions.name] = value;
    } else {
        sort = {};
    }

    var totalCount;

    User.count({}, function (err, count) {
        totalCount = count;
    })

    User.find(req.body.query, {created_by: 0}, {
        'sort': sort,
        skip: skip,
        limit: req.body.paginationOptions.pageSize
    }, function (err, books) {
        return res.json({users: books, total: totalCount});
    });

})

function isAdmin(req, res, next) {
    if (req.isAuthenticated() && req.user._doc.local.role == 'admin')
        return next();
    else
        return response.json({msg: 'You are not admin'});


}

router.get('/isAdmin', function (req, res, next) {
    if (req.isAuthenticated() && req.user._doc.local.role == 'admin')
        return res.status(200).json({api: req.user});

    return res.status(403).json({api: 'access denied'});
});


module.exports = router;