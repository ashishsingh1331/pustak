var express = require('express');
var router = express.Router();
var passport = require('passport');
var Book = require('../models/book');
var common = require('../common');
var User = require('../models/user');
var querystring = require('querystring');
var request = require('request');
var bunyan = require('bunyan');
var log = bunyan.createLogger({name: "index.js"});

/* GET home page. */
router.get('/', function (req, res, next) {

    Book.find({},{created_by:0},function(err,books){
        res.render('index',{books:books});
    });

});/* GET admin page. */
router.get('/admin', function (req, res, next) {

    Book.find({},{created_by:0},function(err,books){
        res.render('adminLanding',{books:books});
    });

});

/* GET login page. */
router.get('/login', isLoggedInRedirect, function (req, res, next) {
    common.getStreamTerms().exec(function(err,jedis){
        if(err)
            return console.log(err);
        res.render('login', {message: req.flash('loginMessage'), terms:jedis.taxonomy_terms,signUpmsg: req.flash('signupMessage')});
    })
});

//TODO- create verify user email link

/* GET dashboard page. */
router.get('/admin/dashboard', function (req, res) {
    if (req.isAuthenticated()){
        res.render('admin/dashboard');
    }
});

router.get('/signup', isLoggedInRedirect, function (req, res, next) {
    res.render('signup', {message: req.flash('signupMessage')});
});

router.get('/profile', isLoggedIn, function (req, res, next) {

    if (req.user._doc.local.role == 'admin') {
        next();
    } else {
        res.render('profile', {user: req.user});
    }

},function(req,res){
    res.redirect('/admin/dashboard');
});

router.get('/logout', function (req, res, next) {
    req.logout();
    res.status(200).json({msg:"loggedOut"});
})

router.post('/signup',isUserAlready,isCaptchaValid, passport.authenticate('local-signup', {
    successRedirect: '/successjson',
    failureRedirect: '/failedSignup',
    failureFlash: true,
}));

router.get('/successjson', function(req, res) {
   return  res.status(200).json({
        msg:"user created"
    });
});

router.get('/failurejson', function(req, res) {
    return  res.status(400).json({
        msg:"Wrong Email/password"
    });
});
router.get('/failedSignup', function(req, res) {
    return  res.status(400).json({
        msg:"Wrong Email/password"
    });
});

router.post('/login',isUserActivated, passport.authenticate('local-login', {
    failureRedirect: '/failurejson',
    successRedirect:'/successLogin',
    failureFlash: true,
}));

function isUserActivated(req, res, next) {
    User.findOne({ 'local.email':  req.body.email }, function(err, user) {



        if (err){
            return  res.status(400).json({
                msg:"Error while fetching user"
            });
        }

        if (user) {

            if(user.local.active == 0){
                return  res.status(400).json({
                    msg:"Your account is not active. Activation link has been sent to your mail"
                });
            }

        }

        if(user == null){
            return  res.status(400).json({
                msg:"Unable to find user"
            });
        }


        return next();
    });
}


function isUserAlready(req, res, next) {
    User.findOne({ 'local.email':  req.body.email }, function(err, user) {



        if (err){
            return  res.status(400).json({
                msg:"Wrong Email/password"
            });
        }

        if (user) {
            return  res.status(400).json({
                msg:"User already"
            });
        } else {
        return next();
        }
    });
}

function isCaptchaValid(req, res, next) {
    var postData = querystring.stringify({
        'secret':googleSecret,
        'response': req.body.myRecaptchaResponse,
        'remoteip':req.connection.remoteAddress
    });


    var url = 'https://www.google.com/recaptcha/api/siteverify';
    var options = {
        method: 'POST',
        body: postData,
        json: true,
        url: url,
        port: '443',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postData)
        }
    }


    request(options, function (err, re, body) {

        if (err) {
            return  res.status(400).json({
                msg:"Wrong Captcha"
            });
        } else{
            if(body.success == true){
                return next();
            } else{
                return  res.status(400).json({
                    msg:"Wrong Captcha"
                });
            }

        }

    })
}

router.get('/successLogin', function(req, res) {




    if(req.user.local.role == "admin"){
        return  res.status(200).json({

        });
    } else {
        return  res.status(200).json({

        });
    }
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}

function isLoggedInRedirect(req, res, next) {
    if (!req.isAuthenticated())
        return next();

    res.redirect('/profile');
}

module.exports = router;
