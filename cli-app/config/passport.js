var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var OTT = require('../models/oneTimeToken');

var common = require('../common.js');
var randtoken = require('rand-token');
var jadeCompiler = require('../lib/jadeCompiler');
var mailcomposer = require('mailcomposer');




module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            roleField:'role',
            contactField:'contact_number',
            stream:'stream',
            location:'location',
            latlng:'latlng',
            passReqToCallback: true,
        },
        function(req, email, password, done) {
            process.nextTick(function() {

                var newUser = new User();
                newUser.local.email = email;
                newUser.local.password = newUser.generateHash(password);
                newUser.local.role = 'authenticated user';
                newUser.local.contact_number = req.body.contact_number ;
                //newUser.local.stream = req.body.stream ;
                newUser.local.location = req.body.location;
                newUser.local.latlng = req.body.latlng;
                newUser.local.active = 0;
                newUser.local.full_name = req.body.full_name;
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    var token = randtoken.generate(16);
                    var url  = req.protocol + '://' + req.get('host')+'/#/user/verify/'+token;

                    var newOTT = new OTT();
                    newOTT.oneTimeToken = token;
                    newOTT.userId = newUser._id;

                    newOTT.save(function(err){
                        if(err){
                            console.log('error in saving new one time token')
                        } else {
                            console.log('one time token saved')
                        }
                    });


                    var data = {
                        name:req.body.full_name,
                        oneTimeLink:url
                    }


                    // get compiled template first
                    jadeCompiler.compile('email/welcome',data, function(err, html){

                        if(err){
                            throw new Error('Problem compiling template(double check relative path): ' + 'email');
                        }



                        var mail = mailcomposer({
                            from: 'you@samples.mailgun.org',
                            to: email,
                            subject: 'Account Created',
                            body: 'Test email text',
                            html: html
                        });

                        mail.build(function(mailBuildError, message) {

                            var dataToSend = {
                                to: email,
                                message: message.toString('ascii')
                            };

                            mailgun.messages().sendMime(dataToSend, function (sendError, body) {
                                if (sendError) {
                                    console.log(sendError);
                                    return;
                                }
                            });
                        });

                    });

                    return done(null, newUser);
                });
            });
        }));

    passport.use('local-login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
        },
        function(req, email, password, done) {
            User.findOne({ 'local.email':  email }, function(err, user) {
                if (err)
                    return done(err);
                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.'));
                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Wrong password.'));

                return done(null, user);
            });
        }));
};
