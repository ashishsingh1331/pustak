var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var busboy = require('connect-busboy');
var fs = require('fs');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var compression = require('compression');
var bunyan = require('bunyan');
var log = bunyan.createLogger({name: "app.js"});
//Todo-bob write Angularjs unit tests


var Book = require('./models/book');
var common = require('./common');
var gun = require('./mailGunApi');



function handleSayHello(req, res) {
    // Not the movie transporter!

    var transporter = nodemailer.createTransport(smtpTransport({
        service: 'gmail',
        auth: {
            user: 'zendchamp@gmail.com', // my mail
            pass: 'sachin@100'
        }
    }));
    var text = 'Hello world from';

    var mailOptions = {
        from: 'zendchamp@gmail.com', // sender address
        to: 'himrashish@yahoo.com', // list of receivers
        subject: 'Email Example', // Subject line
        text: text //, // plaintext body
        // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
    };

    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log(error);
            res.json({yo: 'error'});
        }else{
            console.log('Message sent: ' + info.response);
            res.json({yo: info.response});
        };
    });
}


var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var flash = require('connect-flash');
var session = require('express-session');

 var configDB = require('./config/database.js');
mongoose.Promise = global.Promise;
 mongoose.connect(configDB.url);




var routes = require('./routes/index');
var users = require('./routes/users');
var admin = require('./routes/admin');
var api = require('./routes/api');

var app = express();
app.use(compression());

var cron = require('cron');


api_key = 'key-f100a40be46f5566d2cb46429f2a1079';
domain = 'padhkbecho.com';
mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});


var cronJob = cron.job('*/20 * * * * *', function(){
    // perform operation e.g. GET request http.get() etc.
    console.info('cron job completed');
    var query = [{"title": /.*s.*/}, {description:"l" }];

    //console.log('list',mailgun.lists('engineering@sandboxfa477c6f8c4a48cbba121248afb99da1.mailgun.org'));
    //
    //common.findSimilarBooks(query).exec(function(err,books){
    //    if(err)
    //        return console.log(err);
    //    console.log(books.length);
    //});
    //common.getUncompletedBookStreams().exec(function(err,requests){
    //    if(err)
    //        return console.log(err);
    //
    //
    //    getSubcribedUsers(requests)
    //
    //
    //});

    //function getSubcribedUsers(requests){
    //    //console.log(requests);
    //
    //    for(var i=0;i < requests.length;i++){
    //
    //        //common.getSubscribedUsers({'local.subscribed_terms':requests[i].catetory }).exec(function(err,users){
    //        //
    //        //    if(err)
    //        //        return console.log(err);
    //        //
    //        //    console.log(users);
    //        //
    //        //});
    //
    //    }
    //}




});
cronJob.start();
var http = require( "http" ).createServer( app );
 io = require( "socket.io" )( http );




clients = {};

io.on('connection', function (socket) {
    socket.on('book request', function (data) {
        console.log(data.email);
    });
    socket.on('join', function (data) {
    log.info(data.email)
        if( io.sockets.adapter.sids[socket.id][data.uid] != true){
            socket.join(data.uid);

            common.getUnreadNotificationCount(data.uid).then(function(count){
                io.sockets.in(data.uid).emit('unreadNotification', {count: count});
            })
        }


         // We are using room of socket io
    });
    socket.on('leave', function (data) {
        console.log('leaving email',data.email);
        socket.leave(data.email);
    });

    //socket.on('disconnect', function() {
    //    log.warn('disconnecting');
    //    //socket.leave(socket.room);
    //});
});
//
//app.use(function(req, res, next) {
//    req.io = io;
//    next();
//});





app.use(session({secret:'sshsecret'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use(function(req,res,next){
    res.locals.login = req.isAuthenticated();
    next();
});

require('./config/passport')(passport);



var book = {
    name: 'Practical Node.js',
    publisher: 'Appress',
    keyword: 'node.js express.js'
};

process.env.NODE_ENV = 'production';
console.log(app.get('env'));

//if(app.get('env') == 'development'){
 //googleSecret = '6LfelAoUAAAAAAgx7MfYa13buceUB5dfKIaIzP4e';
   // http.listen(8080, "localhost");
//} else{
   googleSecret = '6LeKFgoUAAAAACs1vEFBdqLXeXwGhZuIHGfM5XBY';
    http.listen(8080, "padhkbecho.com");
//}



//app.disable('x-powered-by');


// view engine setup
app.set('views',[ path.join(__dirname, 'views'),path.join(__dirname, 'views/angularPartials'),path.join(__dirname, 'views/adminViews/angularPartials')]);
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

app.use('/css',express.static(__dirname + 'public/css'));
app.use('/img',express.static(__dirname+'public/images'));
app.use('/js',express.static(__dirname + 'public/javascripts'));

//app.use('/bookimage',express.static(__dirname + 'public/images/books'));

//app.use(require('less-middleware')(path.join(__dirname, '/public')));

var lessMiddleware = require('less-middleware');



//app.use(express.static(__dirname + '/public'));


app.use('/static', express.static(__dirname + '/public'));
app.use('/angular',express.static(__dirname + '/../node_modules/angular'));
app.use('/angularBar',express.static(__dirname + '/../node_modules/angular-loading-bar/src'));
app.use('/growl',express.static(__dirname + '/../node_modules/angular-growl-v2/build'));
app.use('/bootstrap',express.static(__dirname + '/../node_modules/bootstrap/dist'));
app.use('/afu',express.static(__dirname + '/../node_modules/angular-file-upload/dist'));
app.use('/places',express.static(__dirname + '/../node_modules/ng-autocomplete/src'));
app.use('/cookies',express.static(__dirname + '/../node_modules/angular-cookies'));
app.use('/socket',express.static(__dirname + '/../node_modules/socket.io'));
app.use('/socket-angular',express.static(__dirname + '/../node_modules/angular-socket-io'));
app.use('/underscore',express.static(__dirname + '/../node_modules/underscore'));
app.use('/angular-validation',express.static(__dirname + '/../node_modules/angular-validation/dist'));
app.use('/ui-router',express.static(__dirname + '/../node_modules/angular-ui-router/release'));
app.use('/gn',express.static(__dirname + '/../node_modules/gentelella'));
app.use('/grid',express.static(__dirname + '/../node_modules/angular-ui-grid'));
app.use('/am',express.static(__dirname + '/../node_modules/angular-masonry'));
app.use('/infinite',express.static(__dirname + '/../node_modules/ng-infinite-scroll/build'));
app.use('/bower',express.static(__dirname + '/../bower_components'));
app.use('/ui-boot',express.static(__dirname + '/../node_modules/angular-ui-bootstrap/dist'));
app.use('/captcha',express.static(__dirname + '/../node_modules/angular-recaptcha/release'));
app.use('/animate',express.static(__dirname + '/../node_modules/angular-animate/'));
app.use('/build',express.static(__dirname + '/../build'));
//app.use('/static',lessMiddleware(__dirname + '/public'));
app.use(busboy({
    immediate:true
}));

app.use('/', routes);
app.use('/users', users);
app.use('/admin', admin);
app.use('/api', api);

//TODO-bob create email templates from mailchimp for 1) Email Verfication 2) password reset & create password reset functionality
//TODO-bob   create email templates from mailchimp  2) password reset & create password reset functionality
//TODO-bob   create email templates from mailchimp  3) New books uploads
//TODO-bob   create email templates from mailchimp  4) New books requests
//TODO-bob   create email templates from mailchimp  5) Person wants to buy your book email


app.all('api/*',function(req, res, next){

//Origin is the HTML/AngularJS domain from where the ExpressJS API would be called
    res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

//make sure you set this parameter and make it true so that AngularJS and Express are able to exchange session values between each other
    res.header("Access-Control-Allow-Credentials", "true");
    next();

});



var users = {
    'ashish':{
        email:'ashishsingh1331@gmail.com',
        website:'drupalbaba.com',

    }
};

var findUserByUsername = function(username,callback){

    if(!users[username]){
        return callback(
            new Error(
                'No user matching' + username
            )
        );
    }

    return callback(null,users[username]);
}

app.get('/v1/users/:username',function(request,response,next){
    var username =  request.params.username;

    findUserByUsername(username,function(error,user){

        if(error != null){
            return next(error);
        }
        return response.send('user',user);
    });
});

app.post('/sendMail', handleSayHello);

app.get('/render', function(request, response){
    response.locals = {title:'title pro'};
    response.status(200);
    response.render('render');
});

app.get('/angularPartials/:tmpl', function(request, response){
    var tmpl = request.params.tmpl;
    response.render(tmpl);
});
app.get('/adminViews/angularPartials/:tmpl', function(request, response){
    var tmpl = request.params.tmpl;
    response.render(tmpl);
});




app.get('/params/:role/:name/:status', function(req, res) {
    console.log(req.params);
    console.log(req.route);
    res.end();
});


//
//app.use('/upload',function(request,response){
//
//    request.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
//        file.on('data', function(data){
//            fs.writeFile('upload' + fieldname + filename, data);
//        });
//        file.on('end', function(){
//            console.log('File' + filename + 'is ended');
//        });
//
//    });
//
//    request.busboy.on('finish', function(){
//        console.log('Busboy is finished');
//        response.status(201).end();
//    });
//
//});





// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    // Do logging and user-friendly error message display
    //console.error(err);
    //res.status(500).send({status:500, message: 'internal error', type:'internal'});

    res.render('error', {
        message: err.message,
        error: err
    });
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
