/**
 * Created by ashish on 26/10/16.
 */


$(document).ready(function(){
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
            //console.log('close to bottom')
            $('.loadMoreButton').trigger('click');
        }
    });
});