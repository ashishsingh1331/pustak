/**
 * Created by ashish on 27/9/16.
 */
app.controller('landingCtrl', ['$scope', '$stateParams', 'service', 'mySocket', function (_, $stateParams, service, mySocket) {


    //mySocket.on('book request', function () {
    //    console.log('book added');
    //});

}]);


app.controller('profileCtrl', ['$scope', 'mySocket', 'growl', function (_, mySocket, growl) {


}]);

app.controller('notificationModalCtrl', ['$scope', 'growl','detail','$uibModalInstance', function (_, growl,detail,$uibModalInstance) {

    var vm = this;
        vm.detail = detail;

    switch(detail.message.verb) {
        case "MESSAGE":
            vm.title="Message";
            break;
        case "LIKE":
            vm.title="Liked";
            break;
        default:

    }
    vm.cancel = $uibModalInstance.dismiss;

}]);

app.controller('realTimeStreamCtrl', ['$scope', 'mySocket', function (_, mySocket) {
    _.bookRequests = [];
}]);

app.controller('MyModalController', MyModalController)

function MyModalController($uibModalInstance, items, service) {
    console.log('modal controller');
    var vm = this;
    vm.content = items;
    console.log(items);
    vm.confirm = function () {
        service.executePost(service.withBaseUrl('/message'), {
            toUser: items.toUser,
            message: vm.message,
            bookId: items.bookId
        }, function (data) {
            vm.cancel();
        })
    };
    vm.cancel = $uibModalInstance.dismiss;
};

app.controller('loginCtrl', ['$scope', 'service', '$window', 'Auth', 'mySocket', function (_, service, $window, Auth, mySocket) {

    _.data = {};

    _.signIn = function () {

        service.executePost('/login', _.data, function (data) {
            //growl.success(data.data.msg)
            if (200 == data.status) {
                //fetching user object
                service.executeGet(service.withBaseUrl('/isLoggedIn'), {}, function (data) {

                    mySocket.emit('join', {email: data.data.api.local.email,uid: data.data.api._id});

                    Auth.set(data.data.api);

                    if (data.data.api.local.role == "admin") {
                        var url = "http://" + $window.location.host + "/admin#/admin/profile";
                    } else {
                        //TODO-bob Redirect user on first login to user edit form, it encourage user to fill more fields
                        var url = "http://" + $window.location.host + "/#/user/profile";
                    }

                    $window.location.href = url;
                });


            }
        });
    }

}]);

app.controller('bodyCtrl', ['$scope', 'service', '$rootScope', '$state', '$window', 'mySocket', 'growl', function (_, service, $rootScope, $state, $window, mySocket, growl) {

    init();

    mySocket.on("new_msg", function (data) {
        console.log('particular message', data.msg);
    });
    mySocket.on("bookShortListed", function (data) {
        console.log('bookShortListed message', data.msg);
        growl.success(data.msg);
    });
    mySocket.on("message_received", function (data) {
        console.log('bookShortListed message', data.msg);
        growl.success(data.msg);
    });
    mySocket.on("unreadNotification", function (data) {
        //console.log('unread', data.count);
        $rootScope.unreadNotifications = data.count;
    });

    _.$on('$destroy', function () {
        mySocket.removeListener();
    });


    function init() {
        service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
            $rootScope.streamTerms = data.data.terms;
        });
    }

    _.isOnLogin = function () {
        return $state.includes('login') || $state.includes('signup');
    }


}]);

app.controller('filterCtrl', ['$scope', 'terms', function (_, terms) {
    _.terms = terms;
}]);
app.controller('searchCtrl', ['$scope', '$stateParams', function (_, $stateParams) {

}]);

app.controller('verifyCtrl', ['$scope', 'verify', function (_, verify) {
    console.log(verify);
}]);
app.controller('requestBookCtrl', ['$scope', 'terms', 'service', 'growl', function (_, terms, service, growl) {
    _.terms = terms;
    _.data = {};

    //socket.on('book request', function (data) {
    //    console.log('my book request',data);
    //growl.info(data);
    //});

    _.requestBook = function () {
        service.executePost(service.withBaseUrl('/book/request'), _.data);
    }
}]);

app.controller('headerCtrl', ['$scope', '$rootScope', 'service', '$state', function (_, $rootScope, service, $state) {
    _.notifications = [];

    _.toggled = function (open) {
        if(open){
            service.executePost(service.withBaseUrl('/getNotifications'), _.data, function (data) {
                if (200 == data.status) {
                    //console.log(data.data.notification);
                    _.notifications = data.data.notification
                }
            });
        }

    }
    _.search = function () {
        $state.go("searchResult", {param: _.searchParam});

        //var query = {};
        //query.title = _.searchParam;
        //
        //service.executeGet(service.withBaseUrl('/search'),query,function(data){
        //    _.terms =  data.data.terms;
        //});
    }

}]);


app.controller('signUpCtrl', ['$scope', 'service', 'growl', '$window', '$state', 'vcRecaptchaService', function (_, service, growl, $window, $state, vcRecaptchaService) {


    service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
        _.terms = data.data.terms;
    });

    _.options = {
        types: '(regions)',
        country: 'in'
    }

    _.errorInEmail = function (createUser) {
        return !createUser.email.$valid && createUser.email.$dirty
    }

    _.openToolip = function (valid) {
        if (valid == undefined) {
            return false;
        } else {
            valid;
        }

    }


    function checkPwd(str) {
        if (str.length < 6) {
            return ("too_short");
        } else if (str.length > 50) {
            return ("too_long");
        } else if (str.search(/\d/) == -1) {
            return ("no_num");
        } else if (str.search(/[a-zA-Z]/) == -1) {
            return ("no_letter");
        } else if (str.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {
            return ("bad_char");
        }
        return "string password";
    }


    _.patternMessage = function (valid) {
        //console.log(valie);
        if (!valid) {
            return 'Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Characte';
        } else {
            return 'Strong Password';
        }
    }


    _.signUp = function () {


        _.data.latlng = {
            lat: _.details.geometry.location.lat(),
            lang: _.details.geometry.location.lng()
        }
        service.executePost('/signup', _.data, function (data) {
            if (200 == data.status) {
                growl.success('Verification email is sent to your email.');
                $state.go('logout');
            }
        });
    }

}]);

app.controller('postBookCtrl', ['$scope', 'terms', 'FileUploader', 'service', 'growl', function (_, terms, FileUploader, service, growl) {
    _.data = {};
    _.terms = terms;
    _.uploader = new FileUploader({
        alias: 'image',
        url: service.withBaseUrl('/upload/image'),
        formData: [1, 2, 3],
        onSuccessItem: function (item, response, status, headers) {

            _.data.img_info = response;

            service.executePost(service.withBaseUrl('/book/create'), _.data);
        },
        withCredentials: true
    });

    _.postBook = function () {
        _.uploader.uploadAll();
    }


}]);