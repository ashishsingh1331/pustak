/**
 * Created by ashish on 27/9/16.
 */

var app = angular.module('adminApp', [
    'ui.router',
    'angular-loading-bar',
    'angular-growl',
    'angularFileUpload',
    'ngAutocomplete',
    'ngCookies',
    'validation',
    'validation.rule',
    'ServiceModule',
    'ui.grid',
    'ui.grid.pagination',
    'ui.grid.selection',
    'ui.bootstrap',
]);

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, growlProvider,$validationProvider) {
    growlProvider.globalPosition('bottom-left');
    growlProvider.onlyUniqueMessages(false);
    growlProvider.globalTimeToLive({success: 2000, error: 2000, warning: 3000, info: 6000});
    $httpProvider.interceptors.push('authInterceptorService');
    $urlRouterProvider.otherwise("/");

    $validationProvider.setErrorHTML(function (msg) {
        return  "<label class=\"control-label has-error\"><b class=\"help-block\">" + msg + "</b></label>";
    });




    var adminLanding = {
        url: '/admin/profile',
        name: 'adminLanding',
        type:'admin',
        //templateUrl: "/adminViews/angularPartials/bookList",
        views: {

            content: {
                templateUrl: "/adminViews/angularPartials/adminBookList",
                controller: "bookListCtrl",
                resolve: {
                    // Example using function with simple return value.
                    // Since it's not a promise, it resolves immediately.
                    data: function (service, $stateParams) {

                        //var term = $stateParams.term, query = {};
                        //
                        //if (term.length != 0) {
                        //    query.category = term
                        //}
                        return service.executeGet(service.withBaseUrl('/fetchBooks'), {}, function (data) {

                            return data.data.books;
                        });
                    },
                    totalPending: function (service, $stateParams) {
                        var params = {
                            query:{
                                status:'PENDING'
                            }
                        }

                        return service.executePost(service.withBaseUrl('/fetchTotalPending'), params, function (data) {

                            return data.data.total;
                        });
                    },
                }
            }
        }
    };

var userListing = {
        url: '/admin/userListing',
        name: 'userListing',
        type:'admin',
        //templateUrl: "/adminViews/angularPartials/bookList",
        views: {

            content: {
                templateUrl: "/adminViews/angularPartials/adminUserList",
                controller: "userListCtrl",
                resolve: {

                    totalPending: function (service, $stateParams) {
                        var params = {
                            query:{
                                "local.active":0
                            }
                        }

                        return service.executePost(service.withBaseUrl('/fetchTotalPendingUsers'), params, function (data) {

                            return data.data.total;
                        });
                    },
                }
            }
        }
    };

    var taxonomyListing = {
        url: '/admin/taxonomyListing',
        name: 'taxonomyListing',
        type:'admin',
        //templateUrl: "/adminViews/angularPartials/bookList",
        views: {
            content: {
                templateUrl: "/adminViews/angularPartials/admintaxonomyList",
                controller: "taxonomyListCtrl",
                resolve: {
                    taxonomies: function (service, $stateParams) {
                        return service.executePost(service.withBaseUrl('/adminFetchTaxonomies'), {}, function (data) {
                            return data.data.taxonomies;
                        });
                    },
                }
            }
        }
    };


    var logout = {
        name: 'logout',
        url: '/user/logout',
        resolve: {
            // Example using function with simple return value.
            // Since it's not a promise, it resolves immediately.
            terms: function (service, Auth, $state,$window) {
                return service.executeGet('/logout', {}, function (data) {
                    Auth.remove();
                    //$state.go('login');
                    var url = "http://" + $window.location.host + "/#/user/login";
                    $window.location.href = url;
                });
            },
        },
    };

    $stateProvider
        .state(adminLanding)
        .state(userListing)
        .state(taxonomyListing)
        .state(logout);



});


app.run(['$state', '$cookies', '$rootScope', 'service', 'Auth', '$state','$window', function ($state, $cookies, $rootScope, service, Auth, $state,$window) {


    $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {

        if (angular.isDefined(toState.type) && toState.type == 'admin') {
            service.executeGet(service.withBaseUrl('/isAdmin'), {}, function (data) {
                Auth.set(data.data.api);
                $rootScope.user = data.data.api;
            }, function () {
                Auth.remove();
                //$state.go('login');
                var url = "http://" + $window.location.host + "/#/user/login";
                $window.location.href = url;
            });

        }

    });

    $rootScope.$on('$stateChangeSuccess',
        function (event, toState, toParams, fromState, fromParams) {

            if(angular.isDefined($cookies.get('current.user'))){
                $rootScope.login = true;
            } else {
                $rootScope.login = false;
            }
        });
}]);