/**
 * Created by ashish on 16/10/16.
 */
app.controller('bookListCtrl',['$scope','data','uiGridConstants','service','totalPending','$log',function($scope,data,uiGridConstants,service,totalPending,$log){
    //_.books = data;
    //_.myData = [{name: "Moroni", age: 50},
    //    {name: "Tiancum", age: 43},
    //    {name: "Jacob", age: 27},
    //    {name: "Nephi", age: 29},
    //    {name: "Enos", age: 34}];

    var paginationOptions = {
        pageNumber: 1,
        pageSize: 25,
        sort: null
    };



    $scope.totalPending = totalPending;


    $scope.gridOptions = {
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        useExternalPagination: true,
        useExternalSorting: true,
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 35,
        rowHeight: 35,
        showGridFooter:true,
        columnDefs: [
            { name: 'title' },
            { name: 'price', enableSorting: true },
            { name: 'category', enableSorting: true },
            { name: 'status', enableSorting: true },
            { name: 'author', enableSorting: true }
        ],
        onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
            $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
                if (sortColumns.length == 0) {
                    paginationOptions.sort = null;
                } else {
                    paginationOptions.sort = sortColumns[0].sort.direction;
                    paginationOptions.name = sortColumns[0].colDef.name;
                }
                getPage();
            });
            gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                paginationOptions.pageNumber = newPage;
                paginationOptions.pageSize = pageSize;
                getPage();
            });

            gridApi.selection.on.rowSelectionChanged($scope,function(row){
                var msg = 'row selected ' + row.isSelected;
                $log.log(msg);
                if(row.isSelected)
                    $scope.userObj = row.entity;
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                var msg = 'rows changed ' + rows.length;
                $log.log(msg);
            });
        }
    };
    $scope.gridOptions.multiSelect = true;
    $scope.getPage = getPage;

    var getPage = function() {
        var url;
        switch(paginationOptions.sort) {
            case uiGridConstants.ASC:
                url = '/data/100_ASC.json';
                console.log(paginationOptions.name);
                break;
            case uiGridConstants.DESC:
                console.log(paginationOptions.name);
                url = '/data/100_DESC.json';
                break;
            default:
                url = '/data/100.json';
                break;
        }


       var params = {
            query:{
                status:'PENDING'
            },
           paginationOptions:paginationOptions
        }

        return service.executePost(service.withBaseUrl('/fetchBooks'), params, function (data) {
            $scope.gridOptions.totalItems = data.data.total;
            //var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
            $scope.gridOptions.data = data.data.books;
            $scope.bookObj = data.data.books[0];
        });
    };

    $scope.approveBook = function(bookId){

        service.executePost(service.withBaseUrl('/approveBook'), {_id:bookId}, function (data) {
            $scope.totalPending--;
            getPage();
        });
    }
    $scope.deleteBook = function(bookId){

        service.executePost(service.withBaseUrl('/deleteBook'), {_id:bookId}, function (data) {
            $scope.totalPending--;
            getPage();
        });
    }
    $scope.deleteBook = function(bookId){

        service.executePost(service.withBaseUrl('/deleteBook'), {_id:bookId}, function (data) {
            $scope.totalPending--;
            getPage();
        });
    }
    getPage();


}]);
app.controller('taxonomyListCtrl',['$scope','taxonomies','$log',function($scope,taxonomies,$log){
    $scope.taxonomies = taxonomies;

}]);

app.controller('userListCtrl',['$scope','totalPending','uiGridConstants','service','$log',function($scope,totalPending,uiGridConstants,service,$log){
    $scope.totalPending = totalPending;

    var paginationOptions = {
        pageNumber: 1,
        pageSize: 25,
        sort: null
    };



    $scope.totalPending = totalPending;


    $scope.gridOptions = {
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        useExternalPagination: true,
        useExternalSorting: true,
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 35,
        rowHeight: 35,
        showGridFooter:true,
        columnDefs: [
            { name: 'local.full_name' },

        ],
        onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
            $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
                if (sortColumns.length == 0) {
                    paginationOptions.sort = null;
                } else {
                    paginationOptions.sort = sortColumns[0].sort.direction;
                    paginationOptions.name = sortColumns[0].colDef.name;
                }
                getPage();
            });
            gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                paginationOptions.pageNumber = newPage;
                paginationOptions.pageSize = pageSize;
                getPage();
            });

            gridApi.selection.on.rowSelectionChanged($scope,function(row){
                var msg = 'row selected ' + row.isSelected;
                $log.log(msg);
                if(row.isSelected)
                    $scope.userObj = row.entity;
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                var msg = 'rows changed ' + rows.length;
                $log.log(msg);
            });
        }
    };
    $scope.gridOptions.multiSelect = true;
    $scope.getPage = getPage;

    var getPage = function() {
        var url;
        switch(paginationOptions.sort) {
            case uiGridConstants.ASC:
                url = '/data/100_ASC.json';
                console.log(paginationOptions.name);
                break;
            case uiGridConstants.DESC:
                console.log(paginationOptions.name);
                url = '/data/100_DESC.json';
                break;
            default:
                url = '/data/100.json';
                break;
        }


        var params = {
            query:{
                "local.active":0,
                "local.role": {'$ne':'admin' }
            },
            paginationOptions:paginationOptions
        }

        return service.executePost(service.withBaseUrl('/fetchUsers'), params, function (data) {
            $scope.gridOptions.totalItems = data.data.total;
            //var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
            $scope.gridOptions.data = data.data.users;
            $scope.userObj = data.data.users[0];
        });
    };

    $scope.approveUser = function(userId){

        service.executePost(service.withBaseUrl('/approveUser'), {_id:userId}, function (data) {
            $scope.totalPending--;
            getPage();
        });
    }
    $scope.deleteUser = function(userId){

        service.executePost(service.withBaseUrl('/deleteUser'), {_id:userId}, function (data) {
            $scope.totalPending--;
            getPage();
        });
    }
    getPage();
}]);