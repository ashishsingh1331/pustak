/**
 * Created by ashish on 16/10/16.
 */
var bookDetail = function (service,$log,$state) {

    return {
        restrict: 'E',
        link: link,
        scope:{
            bookObj:'=',
            gridOptions:'=',
            gridApi:'=',
            getPage:'&'
        },
        templateUrl: '/adminViews/angularPartials/bookDetail'
    };

    function link(scope, element, attr) {
        //service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
        //    scope.streamTerms =  data.data.terms;
        //});
        scope.approveBook = function(bookId){

            service.executePost(service.withBaseUrl('/approveBook'), {_id:bookId}, function (data) {
                //var i = scope.gridOptions.data.indexOf(scope.bookObj);
                //scope.gridOptions.data.splice(i, 1);
                //$state.reload();
                scope.getPage;
            });
        }
    }

}
var myEnter = function (service,$state,growl) {

    return {
        restrict: 'A',
        link: link,
        scope: {
            taxonomyTitle:'=',
            termText:'=',
            taxonomies:'=',
            parentIndex:'='
        },
    };

    function link(scope, element, attr) {

        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {

                if(scope.taxonomies[scope.parentIndex].taxonomy_terms.indexOf(scope.termText) !== -1) {
                     growl.error('Already exists');
                    return;
                }

                service.executePost(service.withBaseUrl('/taxonomy/term/add'), {taxonomy_title:scope.taxonomyTitle,term:scope.termText}, function (data) {

                    $state.go('taxonomyListing', {}, {reload: true})
                });


                event.preventDefault();
            }
        });
    }

}
app.directive('bookDetail', ['service','$log','$state',bookDetail]);
app.directive('myEnter',['service','$state','growl',myEnter]);