/**
 * Created by ashish on 27/9/16.
 */

var app = angular.module('angularApp', [
    'ui.router',
    'angular-loading-bar',
    'angular-growl',
    'angularFileUpload',
    'ngAutocomplete',
    'ngCookies',
    'validation',
    'validation.rule',
    'ServiceModule',
    //'wu.masonry',
    //'infinite-scroll',
    'ui.bootstrap',
    'vcRecaptcha',
    'btford.socket-io',
    'ngAnimate'
]);

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, growlProvider, $validationProvider, socketFactoryProvider,$locationProvider) {
    growlProvider.globalPosition('bottom-left');
    growlProvider.onlyUniqueMessages(true);
    growlProvider.globalTimeToLive({success: 2000, error: 2000, warning: 3000, info: 6000});
    $httpProvider.interceptors.push('authInterceptorService');
    $urlRouterProvider.otherwise("/");

    $validationProvider.setErrorHTML(function (msg) {
        return "<label class=\"control-label has-error\"><b class=\"help-block\">" + msg + "</b></label>";
    });

    var landing = {
        url: '/:term',
        name: 'landing',
        templateUrl: "angularPartials/bookList",
        controller: "landingCtrl",
    };


    var login = {
        name: 'login',
        url: '/user/login',
        templateUrl: "angularPartials/loginUser",
        controller: "loginCtrl",
    };
    var signup = {
        name: 'signup',
        url: '/user/signup',
        templateUrl: "angularPartials/createUser",
        controller: "signUpCtrl",
        resolve: {
            // Example using function with simple return value.
            // Since it's not a promise, it resolves immediately.
            terms: function (service) {
                return service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
                    return data.data.terms;
                });
            },
        }
    };
    var security = function (service, $state, Auth, $rootScope) {
        return service.executeGet(service.withBaseUrl('/isLoggedIn'), {}, function (data) {
            Auth.set(data.data.api);
            $rootScope.user = data.data.api;
        }, function () {
            Auth.remove();
            $state.go('login');
        });
    };


    var postBook = {
        name: 'postBook',
        url: '/post/book',
        type: 'private',
        templateUrl: "angularPartials/postBook",
        controller: "postBookCtrl",
        resolve: {
            // Example using function with simple return value.
            // Since it's not a promise, it resolves immediately.
            terms: function (service) {
                return service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
                    return data.data.terms;
                });
            },
            secure: security,
        },
    };

    var requestBook = {
        name: 'requestBook',
        url: '/request/book',
        type: 'private',

        templateUrl: "angularPartials/requestBook",
        controller: "requestBookCtrl",
        resolve: {
            // Example using function with simple return value.
            // Since it's not a promise, it resolves immediately.
            terms: function (service) {
                return service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
                    return data.data.terms;
                });
            },
            secure: security,
        },

    };

    var profile = {
        name: 'profile',
        url: '/user/profile',
        type: 'private',
        resolve: {
            secure: security,
        },
        templateUrl: "angularPartials/profile",
        controller: "profileCtrl",
    };

    var subscribe = {
        name: 'subscribe',
        url: '/user/subscribe',
        type: 'private',
        resolve: {
            secure: security,
        },
        templateUrl: "angularPartials/subscribe",
    };
    var verify = {
        name: 'verify',
        url: '/user/verify/:oneTimeToken',
        //templateUrl: "angularPartials/profile",
        //controller:'profileCtrl',
        views: {
            content: {
                templateUrl: "angularPartials/verify",
                controller: "verifyCtrl",
                resolve: {
                    verify: function (service, $stateParams, growl, $state) {

                        var oneTimeToken = $stateParams.oneTimeToken;

                        var query = {
                            oneTimeToken: oneTimeToken
                        }

                        return service.executeGet(service.withBaseUrl('/verifyOTT'), query, function (data) {
                            growl.success(data.data.msg);
                            $state.go('profile');

                        }, function (data) {
                            growl.error(data.data.msg);
                            $state.go('login');
                        });
                    }
                }
            }
        }
    };

    var searchResult = {
        name: 'searchResult',
        url: '/book/search/:param',
        controller: 'searchCtrl',
        templateUrl: "angularPartials/searchResult",
        controller: "searchCtrl",
    };

    var logout = {
        name: 'logout',
        url: '/user/logout',
        resolve: {
            // Example using function with simple return value.
            // Since it's not a promise, it resolves immediately.
            terms: function (service, Auth, $state, mySocket, $rootScope) {
                return service.executeGet('/logout', {}, function (data) {
                    mySocket.emit('leave', {email: $rootScope.user.local.email});
                    Auth.remove();

                    $state.go('login');

                });
            },
        },
    };

    $stateProvider
        .state(landing)
        .state(login)
        .state(signup)
        .state(profile)
        .state(logout)
        .state(verify)
        .state(requestBook)
        .state(subscribe)
        .state(searchResult)
        .state(postBook);

    //$locationProvider.html5Mode(true);


});


app.run(['$state', '$cookies', '$rootScope', 'service', 'Auth', '$state','mySocket', function ($state, $cookies, $rootScope, service, Auth, $state,mySocket) {


    $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {

        if (angular.isDefined(Auth.get())) {
            $rootScope.user = Auth.get();
            mySocket.emit('join', {email: $rootScope.user.local.email,uid: $rootScope.user._id});
            //if (angular.isUndefined($rootScope.unreadNotifications)) {
            //    service.executePost(service.withBaseUrl('/getUnreadNotificationCount'), {}, function (data) {
            //        if (200 == data.status) {
            //            $rootScope.unreadNotifications = data.data.count;
            //        }
            //    });
            //}

        }


    });

    $rootScope.$on('$stateChangeSuccess',
        function (event, toState, toParams, fromState, fromParams) {

            if (angular.isDefined($cookies.get('current.user'))) {
                $rootScope.login = true;
            } else {
                $rootScope.login = false;
            }
        });
}]);