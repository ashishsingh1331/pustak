/**
 * Created by ashish on 27/9/16.
 */
var service = angular.module('ServiceModule', []);
service.factory('service', ['$http',  function ($http) {
    var factory = {};
    var urlBase = '/api';

    factory.executeGet = function (url, data, success, failure) {

        return $http({
            method: 'GET',
            url: url,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json"
            },
            params: data,
            withCredentials: true
        }).then(success,
            failure);

    };

    factory.withBaseUrl = function (url) {
        return urlBase + url;
    }

    factory.executePost = function (url, data, success, failure) {

        return $http({
            method: 'POST',
            url: url,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json"
            },
            data: data,
            withCredentials: true
        }).then(success,
            failure);

    };

    return factory;
}]);

service.factory('authInterceptorService', ['$q','$location','$window','Auth','growl', function ($q, $location,$window,Auth,growl){
    var responseError = function (rejection) {
        if (rejection.status === 403 ||  rejection.status === 401) {
            growl.info('<strong>You need to login to access this page</strong>');
        }
        if (rejection.status === 400) {
            if(angular.isDefined(rejection.data.msg)){
                growl.error(rejection.data.msg);
            }
        }
        return $q.reject(rejection);
    };
    var response = function (response) {

        if (response.status === 200) {
            if(angular.isDefined(response.data.msg)){
                growl.success(response.data.msg);
            }
        }
        return response || $q.when(response);
    };

    return {
        responseError: responseError,
        response:response
    };
}]);

service.factory('Auth', ['$cookieStore', function ($cookieStore) {

    var _user = {};

    return {

        user : _user,

        set: function (_user) {
            // you can retrive a user setted from another page, like login sucessful page.
            existing_cookie_user = $cookieStore.get('current.user');
            _user =  _user || existing_cookie_user;

            $cookieStore.put('current.user', _user);
        },
        get: function (_user) {
            // you can retrive a user setted from another page, like login sucessful page.
            return $cookieStore.get('current.user');

        },

        remove: function () {
            console.log('removing cookie');
            $cookieStore.remove('current.user', _user);
        }
    };
}]);

service.factory('mySocket',['socketFactory','$location' ,function (socketFactory,$location) {
    return socketFactory({
        ioSocket: io.connect($location.protocol() + "://" + $location.host()+':8080')
    });
}]);

function myModalFactory($uibModal) {
    var open = function (size, title, message,toUser,bookId) {
        return $uibModal.open({
            controller: 'MyModalController',
            controllerAs: 'vm',
            templateUrl : 'angularPartials/message.jade',
            size: size,
            resolve: {
                items: function() {
                    return {
                        title: title,
                        message: message,
                        toUser:toUser,
                        bookId:bookId
                    };
                }
            }
        });
    };

    return {
        open: open
    };
}

app.factory('$myModal', myModalFactory)