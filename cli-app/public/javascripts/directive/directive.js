/**
 * Created by ashish on 5/10/16.
 */

var sidebarTerms = function (service) {

    return {
        restrict: 'E',
        link: link,
        scope: true,
        templateUrl: '/angularPartials/sidebarTerms'
    };

    function link(scope, element, attr) {
        service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
            scope.streamTerms = data.data.terms;
        });
    }

}

var subscribeTerms = function (service, $rootScope, $window) {

    return {
        restrict: 'E',
        link: link,
        scope: true,
        templateUrl: '/angularPartials/subscribeTerms'
    };

    function link(scope, element, attr) {
        scope.filter = '';

        scope.subscribed_terms = angular.isDefined($rootScope.user) ? $rootScope.user.local.subscribed_streams : [];

        service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
            scope.streamTerms = data.data.terms;
            scope.unSubscribedTerms = $window._.difference(data.data.terms, scope.subscribed_terms)
        });


        scope.subscribe = function (term) {

            var uScoredTerm = term.split(' ').join('_')

            var data = {
                term: uScoredTerm
            }
            service.executePost(service.withBaseUrl('/subscribe/stream'), data, function (data) {
                scope.subscribed_terms.push(term);
                var index = scope.unSubscribedTerms.indexOf(term);
                scope.unSubscribedTerms.splice(index, 1);
            });
        }

        scope.unSubscribe = function (term) {
            var uScoredTerm = term.split(' ').join('_')

            var data = {
                term: uScoredTerm
            };


            service.executePost(service.withBaseUrl('/unSubscribe/stream'), data, function (data) {
                scope.unSubscribedTerms.push(term);
                var index = scope.subscribed_terms.indexOf(term);
                scope.subscribed_terms.splice(index, 1);
            });
        }
    }
}

var searchResult = function (service, $stateParams, growl, $rootScope) {

    return {
        restrict: 'E',
        link: link,
        //scope: true,
        templateUrl: '/angularPartials/searchResultDirective'
    };

    function link(scope, element, attr) {
        scope.pageNumber = 1;
        scope.pageSize = 15;
        scope.books = [];
        scope.disableButton = false;

        scope.$watch('pageNumber', function (newValue, oldValue) {
            if (newValue != oldValue) {
                scope.searchBooks();
            }
        }, true);
        scope.$watch('searchButtonClicked', function (newValue, oldValue) {
            if (newValue != oldValue && scope.disableButton == false) {
                scope.searchBooks();
            }
        }, true);

        scope.incrementPageNumber = function () {
            if (scope.disableButton == false) {
                scope.pageNumber++;
            }
        }

        scope.likedByLoggedInUser = function (likedByUsers) {
            if (angular.isDefined(($rootScope.user))) {
                return (likedByUsers.indexOf($rootScope.user._id) != -1 ) ? true : false;
            } else {
                return false;
            }
            s

        }

        scope.searchBooks = function () {

            var term = $stateParams.term, query = {}, searchParam = $stateParams.param;


            if (angular.isDefined(term) && term != '') {
                query.category = term
            }

            if (angular.isDefined(searchParam)) {
                query.title = searchParam
            }

            query.status = 'APPROVED';


            var paginationOptions = {
                pageNumber: (scope.pageNumber),
                pageSize: scope.pageSize
            }


            var params = {
                query: query,
                paginationOptions: paginationOptions
            }


            return service.executePost(service.withBaseUrl('/fetchBooks'), params, function (data) {

                if (data.data.books.length == 0) {
                    scope.disableButton = true;
                    return
                }

                scope.books = scope.books.concat(data.data.books);
            });
        }

        scope.searchBooks();

    }
}

function modalTriggerDirective($myModal) {
    function postLink(scope, iElement, iAttrs) {

        function onClick() {
            var size = scope.$eval(iAttrs.size) || 'lg'; // default to large size
            var title = scope.$eval(iAttrs.title) || 'Default Title';
            var message = scope.$eval(iAttrs.message) || 'Default Message';
            var toUser = scope.$eval(iAttrs.touser) || 'Default Message';
            var bookId = scope.$eval(iAttrs.bookid) || 'Default Message';

            $myModal.open(size, title, message, toUser, bookId);
        }

        iElement.on('click', onClick);
        scope.$on('$destroy', function () {
            iElement.off('click', onClick);
        });
    }

    return {
        link: postLink
    };
}


var scrollBody = function (service, $window) {

    return {
        restrict: 'A',
        link: link,
    };

    function link(scope, element, attr) {
        console.log($window);
    }
}

app.directive('setClassWhenAtTop', function ($window) {
    var $win = angular.element($window); // wrap window object as jQuery object

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
                offsetTop = element.offset().top; // get element's offset top relative to document

            $win.on('scroll', function (e) {
                if ($win.scrollTop() >= offsetTop) {
                    element.addClass(topClass);
                } else {
                    element.removeClass(topClass);
                }
            });
        }
    };
});
app.directive('notification', function () {
    return {
        restrict: 'A',
        //templateUrl: '/angularPartials/notifications',
        link: function (scope, element, attrs) {
            function onClick() {


            }

            element.on('click', onClick);
        }
    };
});
app.directive('collapseNav', function () {

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                $(".navbar-collapse").collapse('hide');
            });
        }
    };
});
app.directive('realTimeStream', ['mySocket', function (mySocket) {
    return {
        restrict: 'E',
        templateUrl: '/angularPartials/realTimeStream',
        link: function (scope, element, attrs) {
            scope.bookRequests = [];
            mySocket.on('book request', function (data) {

                scope.bookRequests.push(data);
            });
        }
    };
}]);
app.directive('shortListBook', ['service', function (service) {
    return {
        restrict: 'EA',
        scope: {
            "toUser": '=',
            "bookId": '=',
            "index": '=',
            "books": '='
        },
        link: function (scope, element, attrs) {
            element.bind('click', function () {

                service.executePost(service.withBaseUrl('/shortListBook'), {
                    toUser: scope.toUser,
                    bookId: scope.bookId
                }, function (data) {
                    scope.books[scope.index] = data.data.book;
                });
            });
        }
    };
}]);
app.directive('notificationDetail', ['service', '$uibModal', function (service, $uibModal) {
    return {
        restrict: 'A',
        scope: {
            "notificationId": '@',
            "messageId": '@',
            "bookId": '@',
        },
        link: function (scope, element, attrs) {
            element.bind('click', function () {


                var param = {
                    notificationId:scope.notificationId,
                    messageId:scope.messageId,
                    bookId:scope.bookId
                };

                var open = function (size, title, message, toUser, bookId) {
                    return $uibModal.open({
                        controller: 'notificationModalCtrl',
                        controllerAs: 'vm',
                        templateUrl: 'angularPartials/notificationDetail.jade',
                        size: size,
                        resolve: {
                            detail: function () {
                               return service.executePost(service.withBaseUrl('/notificationDetail'), param, function (data) {
                                    if (200 == data.status) {
                                        return data.data
                                    }
                                });
                            }
                        }
                    });
                };

                open();

            });
        }
    };
}]);

app.filter('reverse', function () {
    return function (items) {
        return items.slice().reverse();
    };
});
app.directive('modalTrigger', modalTriggerDirective)
app.directive('sidebarTerms', sidebarTerms)
app.directive('searchResult', ['service', '$stateParams', 'growl', '$rootScope', searchResult])
app.directive('scrollBody', ['service', '$window', scrollBody])
app.directive('subscribeTerms', ['service', '$rootScope', '$window', subscribeTerms]);