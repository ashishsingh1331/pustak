/**
 * Created by ashish on 30/9/16.
 */
var Taxonomy = require('./models/taxonomy');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var Book = require('./models/book');
var BookRequest = require('./models/bookRequest');
var User = require('./models/user');
var Message = require('./models/message');
var bunyan = require('bunyan');
var log = bunyan.createLogger({name: "common.js"});
var Notification = require('./models/notification');
var Q = require("q");
function Common() {
}

Common.prototype.getStreamTerms = function () {
    return Taxonomy.findOne({taxonomy_title: 'Stream'});
}
Common.prototype.sendMail = function (mailOptions) {

    var transporter = nodemailer.createTransport(smtpTransport({
        service: 'gmail',
        auth: {
            user: 'zendchamp@gmail.com', // my mail
            pass: 'sachin@100'
        }
    }))

    //var mailOptions = {
    //    to: 'himrashish@yahoo.com', // list of receivers
    //    subject: 'Email Example', // Subject line
    //    text: text //, // plaintext body
    //    // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
    //};

    mailOptions.from = 'zendchamp@gmail.com';


    return transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            return {yo: 'error'};
        } else {
            console.log('Message sent: ' + info.response);
            return {yo: info.response};
        }
        ;
    });
}

Common.prototype.findSimilarBooks = function (query) {
    return Book.find({$or: query}, {created_by: 0});
}
Common.prototype.addLikedBooksInUserObject = function (uid, bookId) {
    User.findById(uid, function (err, user) {
        if (err || user == null) {
            log.warn(err);
            return false;
        }
        user.local.liked_books.push(bookId);
        user.save(function (err) {
            if (err) {
                log.warn(err);
                return false;
            }
            return true;

        });


    });
}


Common.prototype.updateBookLikes = function (uLikedId, bookId) {
    return Book.findById(bookId);
}
Common.prototype.saveMessage = function (request) {
    var deferred = Q.defer();

    var message = new Message();      // create a new instance of the Taxonomy model
    message.toUser = request.body.toUser;  // set the Taxonomys name (comes from the request)
    message.fromUser = request.user._id;
    message.message = request.body.message;

    message.save(function (err) {

        if (err) {
            deferred.reject(new Error(err));
        } else {
            deferred.resolve(message);
        }

    });

    // Return the promise that we want to use in our chain
    return deferred.promise;
}
Common.prototype.addNotification = function (request, message) {
    var deferred = Q.defer();

    var notification = new Notification();      // create a new instance of the Taxonomy model
    notification.toUser = request.body.toUser;  // set the Taxonomys name (comes from the request)
    notification.fromUser = request.user._id;
    notification.verb = 'MESSAGE';
    notification.bookId = request.body.bookId;
    notification.messageId = message._id;
    notification.notText =   request.user.local.full_name + ' sent you message ' + '\"'+message.message+'\"';

    notification.save(function (err) {

        if (err) {
            deferred.reject(new Error(err));
        } else {
            deferred.resolve(notification);
        }

    });

    // Return the promise that we want to use in our chain
    return deferred.promise;
}
Common.prototype.findUserById = function (id) {
    var deferred = Q.defer();

    User.findById(id, function (err, p) {
        if (!p)
            deferred.reject(new Error(err));
        else {
            // do your updates here
            deferred.resolve(p);
        }
    });

    // Return the promise that we want to use in our chain
    return deferred.promise;
}
Common.prototype.getNotificaiton = function (id) {
    var deferred = Q.defer();

    Notification.find({toUser: id}, {}, {
        sort: {
            ts: -1 //Sort by Date Added DESC
        },
        limit:10
    }, function (err, p) {
        if (!p)
            deferred.reject(new Error(err));
        else {
            // do your updates here
            deferred.resolve(p);
        }
    });

    // Return the promise that we want to use in our chain
    return deferred.promise;
}
Common.prototype.getNoticationObj = function (id) {
    var deferred = Q.defer();

    Notification.findById(id, function (err, p) {
        if (!p)
            deferred.reject(new Error(err));
        else {
            // do your updates here
            deferred.resolve(p);
        }
    });

    // Return the promise that we want to use in our chain
    return deferred.promise;
}
Common.prototype.getMessageObj = function (id) {
    var deferred = Q.defer();

    Message.findById(id, function (err, p) {
        if (!p)
            deferred.reject(new Error(err));
        else {
            // do your updates here
            deferred.resolve(p);
        }
    });

    // Return the promise that we want to use in our chain
    return deferred.promise;
}
Common.prototype.getBookObj = function (id) {
    var deferred = Q.defer();

    Book.findById(id,{'created_by.local.password': 0}, function (err, p) {
        if (!p)
            deferred.reject(new Error(err));
        else {
            // do your updates here
            deferred.resolve(p);
        }
    });

    // Return the promise that we want to use in our chain
    return deferred.promise;
}
Common.prototype.getUnreadNotificationCount = function (id) {
    var deferred = Q.defer();

    Notification.count({toUser: id,isRead:false}, function (err, count) {
        if (!count)
            deferred.reject(new Error(err));
        else {
            // do your updates here
            deferred.resolve(count);
        }
    });

    // Return the promise that we want to use in our chain
    return deferred.promise;
}
Common.prototype.getUnreadNotificationCount = function (id) {
    var deferred = Q.defer();

    Notification.count({toUser: id,isRead:false}, function (err, count) {
        if (!count)
            deferred.reject(new Error(err));
        else {
            // do your updates here
            deferred.resolve(count);
        }
    });

    // Return the promise that we want to use in our chain
    return deferred.promise;
}
Common.prototype.isAlreadyLiked = function (uLikedId, bookId) {

    return Book.findById(bookId);
}
Common.prototype.getUncompletedBookRequests = function (query) {
    return BookRequest.find({completed: false}, {created_by: 0});
}
Common.prototype.getUncompletedBookStreams = function (query) {
    return BookRequest.find({completed: false});
}
Common.prototype.getSubscribedUsers = function () {
    return User.find({"local.subscribed_streams": {$gt: []}}, {
        "local.subscribed_streams": 1,
        _id: 0,
        "local.email": 1
    });
}

module.exports = new Common();