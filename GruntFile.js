module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        src: 'cli-app/public',
        uglify: {
            my_target: {
                files: {
                    'build/libs/libs.min.js': [
                        'node_modules/angular/angular.js',
                        'node_modules/angular-masonry/node_modules/jquery-bridget/jquery-bridget.js',
                        'node_modules/angular-masonry/node_modules/imagesloaded/node_modules/ev-emitter/ev-emitter.js',
                        'node_modules/angular-masonry/node_modules/masonry-layout/node_modules/outlayer/node_modules/fizzy-ui-utils/node_modules/desandro-matches-selector/matches-selector.js',
                        'node_modules/angular-loading-bar/src/loading-bar.js',
                        'bower_components/fizzy-ui-utils/utils.js',
                        'node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
                        'node_modules/angular-recaptcha/release/angular-recaptcha.min.js',
                        'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
                        'node_modules/underscore/underscore.js',
                        'node_modules/angular-cookies/angular-cookies.js',
                        'node_modules/angular-validation/dist/angular-validation.js',
                        'node_modules/angular-validation/dist/angular-validation-rule.js',
                        "node_modules/ng-autocomplete/src/ngAutocomplete.js",
                        "node_modules/angular-file-upload/dist/angular-file-upload.js",
                        "node_modules/angular-growl-v2/build/angular-growl.js",
                        "node_modules/angular-animate/angular-animate.min.js",
                        "node_modules/bootstrap/dist/js/bootstrap.min.js",
                        "node_modules/angular-ui-router/release/angular-ui-router.js",
                        "cli-app/public/javascripts/angular-ui-router.js",
                        //"cli-app/public/javascripts/**/*.js",
                        //"cli-app/public/javascripts/*.js",
                        "node_modules/socket.io/node_modules/socket.io-client/socket.io.js",
                        "node_modules/angular-socket-io/socket.js",
                    ]
                }
            }



        },
        watch: {
            options: {
                livereload: true,
            },
            scripts: {
                files: ["<%=src%>/javascripts/**/*.js"],
            },
            grunt: {
                files: ["Gruntfile.js"],
                tasks: ["newer:jshint:grunt"]
            },
            jade: {
                files: ["<%=src%>/../views/**/*.jade"],
                //tasks: ["initProps", "newer:copy:partials"]
            },
        },
        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                '<%= grunt.template.today("yyyy-mm-dd") %> */',
            },
            dist: {
                src: ['cli-app/public/javascripts/angularApp.js',
                    'cli-app/public/javascripts/custom.js',
                    'cli-app/public/javascripts/directive/directive.js',
                    'cli-app/public/javascripts/controller/bodyCtrl.js',
                    'cli-app/public/javascripts/controller/contentCtrl.js',
                    'cli-app/public/javascripts/service/service.js'

                    ],
                dest: 'build/concat.js',
            },
        },
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('dev', ['watch', 'uglify', 'build']);
    grunt.registerTask('default', ['dev']);
}