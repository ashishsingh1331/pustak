/*! bag - v1.0.0 - 2016-12-19 */var app = angular.module('angularApp', [
    'ui.router',
    'angular-loading-bar',
    'angular-growl',
    'angularFileUpload',
    'ngAutocomplete',
    'ngCookies',
    'validation',
    'validation.rule',
    'ServiceModule',
    //'wu.masonry',
    //'infinite-scroll',
    'ui.bootstrap',
    'vcRecaptcha',
    'btford.socket-io',
    'ngAnimate'
]);

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, growlProvider, $validationProvider, socketFactoryProvider,$locationProvider) {
    growlProvider.globalPosition('bottom-left');
    growlProvider.onlyUniqueMessages(true);
    growlProvider.globalTimeToLive({success: 2000, error: 2000, warning: 3000, info: 6000});
    $httpProvider.interceptors.push('authInterceptorService');
    $urlRouterProvider.otherwise("/");

    $validationProvider.setErrorHTML(function (msg) {
        return "<label class=\"control-label has-error\"><b class=\"help-block\">" + msg + "</b></label>";
    });

    var landing = {
        url: '/:term',
        name: 'landing',
        templateUrl: "angularPartials/bookList",
        controller: "landingCtrl",
    };


    var login = {
        name: 'login',
        url: '/user/login',
        templateUrl: "angularPartials/loginUser",
        controller: "loginCtrl",
    };
    var signup = {
        name: 'signup',
        url: '/user/signup',
        templateUrl: "angularPartials/createUser",
        controller: "signUpCtrl",
        resolve: {
            // Example using function with simple return value.
            // Since it's not a promise, it resolves immediately.
            terms: function (service) {
                return service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
                    return data.data.terms;
                });
            },
        }
    };
    var security = function (service, $state, Auth, $rootScope) {
        return service.executeGet(service.withBaseUrl('/isLoggedIn'), {}, function (data) {
            Auth.set(data.data.api);
            $rootScope.user = data.data.api;
        }, function () {
            Auth.remove();
            $state.go('login');
        });
    };


    var postBook = {
        name: 'postBook',
        url: '/post/book',
        type: 'private',
        templateUrl: "angularPartials/postBook",
        controller: "postBookCtrl",
        resolve: {
            // Example using function with simple return value.
            // Since it's not a promise, it resolves immediately.
            terms: function (service) {
                return service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
                    return data.data.terms;
                });
            },
            secure: security,
        },
    };

    var requestBook = {
        name: 'requestBook',
        url: '/request/book',
        type: 'private',

        templateUrl: "angularPartials/requestBook",
        controller: "requestBookCtrl",
        resolve: {
            // Example using function with simple return value.
            // Since it's not a promise, it resolves immediately.
            terms: function (service) {
                return service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
                    return data.data.terms;
                });
            },
            secure: security,
        },

    };

    var profile = {
        name: 'profile',
        url: '/user/profile',
        type: 'private',
        resolve: {
            secure: security,
        },
        templateUrl: "angularPartials/profile",
        controller: "profileCtrl",
    };

    var subscribe = {
        name: 'subscribe',
        url: '/user/subscribe',
        type: 'private',
        resolve: {
            secure: security,
        },
        templateUrl: "angularPartials/subscribe",
    };
    var verify = {
        name: 'verify',
        url: '/user/verify/:oneTimeToken',
        //templateUrl: "angularPartials/profile",
        //controller:'profileCtrl',
        views: {
            content: {
                templateUrl: "angularPartials/verify",
                controller: "verifyCtrl",
                resolve: {
                    verify: function (service, $stateParams, growl, $state) {

                        var oneTimeToken = $stateParams.oneTimeToken;

                        var query = {
                            oneTimeToken: oneTimeToken
                        }

                        return service.executeGet(service.withBaseUrl('/verifyOTT'), query, function (data) {
                            growl.success(data.data.msg);
                            $state.go('profile');

                        }, function (data) {
                            growl.error(data.data.msg);
                            $state.go('login');
                        });
                    }
                }
            }
        }
    };

    var searchResult = {
        name: 'searchResult',
        url: '/book/search/:param',
        controller: 'searchCtrl',
        templateUrl: "angularPartials/searchResult",
        controller: "searchCtrl",
    };

    var logout = {
        name: 'logout',
        url: '/user/logout',
        resolve: {
            // Example using function with simple return value.
            // Since it's not a promise, it resolves immediately.
            terms: function (service, Auth, $state, mySocket, $rootScope) {
                return service.executeGet('/logout', {}, function (data) {
                    mySocket.emit('leave', {email: $rootScope.user.local.email});
                    Auth.remove();

                    $state.go('login');

                });
            },
        },
    };

    $stateProvider
        .state(landing)
        .state(login)
        .state(signup)
        .state(profile)
        .state(logout)
        .state(verify)
        .state(requestBook)
        .state(subscribe)
        .state(searchResult)
        .state(postBook);

    //$locationProvider.html5Mode(true);


});


app.run(['$state', '$cookies', '$rootScope', 'service', 'Auth', '$state','mySocket', function ($state, $cookies, $rootScope, service, Auth, $state,mySocket) {


    $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {

        if (angular.isDefined(Auth.get())) {
            $rootScope.user = Auth.get();
            mySocket.emit('join', {email: $rootScope.user.local.email,uid: $rootScope.user._id});
            //if (angular.isUndefined($rootScope.unreadNotifications)) {
            //    service.executePost(service.withBaseUrl('/getUnreadNotificationCount'), {}, function (data) {
            //        if (200 == data.status) {
            //            $rootScope.unreadNotifications = data.data.count;
            //        }
            //    });
            //}

        }


    });

    $rootScope.$on('$stateChangeSuccess',
        function (event, toState, toParams, fromState, fromParams) {

            if (angular.isDefined($cookies.get('current.user'))) {
                $rootScope.login = true;
            } else {
                $rootScope.login = false;
            }
        });
}]);
$(document).ready(function(){
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
            //console.log('close to bottom')
            $('.loadMoreButton').trigger('click');
        }
    });
});
var sidebarTerms = function (service) {

    return {
        restrict: 'E',
        link: link,
        scope: true,
        templateUrl: '/angularPartials/sidebarTerms'
    };

    function link(scope, element, attr) {
        service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
            scope.streamTerms = data.data.terms;
        });
    }

}

var subscribeTerms = function (service, $rootScope, $window) {

    return {
        restrict: 'E',
        link: link,
        scope: true,
        templateUrl: '/angularPartials/subscribeTerms'
    };

    function link(scope, element, attr) {
        scope.filter = '';

        scope.subscribed_terms = angular.isDefined($rootScope.user) ? $rootScope.user.local.subscribed_streams : [];

        service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
            scope.streamTerms = data.data.terms;
            scope.unSubscribedTerms = $window._.difference(data.data.terms, scope.subscribed_terms)
        });


        scope.subscribe = function (term) {

            var uScoredTerm = term.split(' ').join('_')

            var data = {
                term: uScoredTerm
            }
            service.executePost(service.withBaseUrl('/subscribe/stream'), data, function (data) {
                scope.subscribed_terms.push(term);
                var index = scope.unSubscribedTerms.indexOf(term);
                scope.unSubscribedTerms.splice(index, 1);
            });
        }

        scope.unSubscribe = function (term) {
            var uScoredTerm = term.split(' ').join('_')

            var data = {
                term: uScoredTerm
            };


            service.executePost(service.withBaseUrl('/unSubscribe/stream'), data, function (data) {
                scope.unSubscribedTerms.push(term);
                var index = scope.subscribed_terms.indexOf(term);
                scope.subscribed_terms.splice(index, 1);
            });
        }
    }
}

var searchResult = function (service, $stateParams, growl, $rootScope) {

    return {
        restrict: 'E',
        link: link,
        //scope: true,
        templateUrl: '/angularPartials/searchResultDirective'
    };

    function link(scope, element, attr) {
        scope.pageNumber = 1;
        scope.pageSize = 15;
        scope.books = [];
        scope.disableButton = false;

        scope.$watch('pageNumber', function (newValue, oldValue) {
            if (newValue != oldValue) {
                scope.searchBooks();
            }
        }, true);
        scope.$watch('searchButtonClicked', function (newValue, oldValue) {
            if (newValue != oldValue && scope.disableButton == false) {
                scope.searchBooks();
            }
        }, true);

        scope.incrementPageNumber = function () {
            if (scope.disableButton == false) {
                scope.pageNumber++;
            }
        }

        scope.likedByLoggedInUser = function (likedByUsers) {
            if (angular.isDefined(($rootScope.user))) {
                return (likedByUsers.indexOf($rootScope.user._id) != -1 ) ? true : false;
            } else {
                return false;
            }
            s

        }

        scope.searchBooks = function () {

            var term = $stateParams.term, query = {}, searchParam = $stateParams.param;


            if (angular.isDefined(term) && term != '') {
                query.category = term
            }

            if (angular.isDefined(searchParam)) {
                query.title = searchParam
            }

            query.status = 'APPROVED';


            var paginationOptions = {
                pageNumber: (scope.pageNumber),
                pageSize: scope.pageSize
            }


            var params = {
                query: query,
                paginationOptions: paginationOptions
            }


            return service.executePost(service.withBaseUrl('/fetchBooks'), params, function (data) {

                if (data.data.books.length == 0) {
                    scope.disableButton = true;
                    return
                }

                scope.books = scope.books.concat(data.data.books);
            });
        }

        scope.searchBooks();

    }
}

function modalTriggerDirective($myModal) {
    function postLink(scope, iElement, iAttrs) {

        function onClick() {
            var size = scope.$eval(iAttrs.size) || 'lg'; // default to large size
            var title = scope.$eval(iAttrs.title) || 'Default Title';
            var message = scope.$eval(iAttrs.message) || 'Default Message';
            var toUser = scope.$eval(iAttrs.touser) || 'Default Message';
            var bookId = scope.$eval(iAttrs.bookid) || 'Default Message';

            $myModal.open(size, title, message, toUser, bookId);
        }

        iElement.on('click', onClick);
        scope.$on('$destroy', function () {
            iElement.off('click', onClick);
        });
    }

    return {
        link: postLink
    };
}


var scrollBody = function (service, $window) {

    return {
        restrict: 'A',
        link: link,
    };

    function link(scope, element, attr) {
        console.log($window);
    }
}

app.directive('setClassWhenAtTop', function ($window) {
    var $win = angular.element($window); // wrap window object as jQuery object

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
                offsetTop = element.offset().top; // get element's offset top relative to document

            $win.on('scroll', function (e) {
                if ($win.scrollTop() >= offsetTop) {
                    element.addClass(topClass);
                } else {
                    element.removeClass(topClass);
                }
            });
        }
    };
});
app.directive('notification', function () {
    return {
        restrict: 'A',
        //templateUrl: '/angularPartials/notifications',
        link: function (scope, element, attrs) {
            function onClick() {


            }

            element.on('click', onClick);
        }
    };
});
app.directive('collapseNav', function () {

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                $(".navbar-collapse").collapse('hide');
            });
        }
    };
});
app.directive('realTimeStream', ['mySocket', function (mySocket) {
    return {
        restrict: 'E',
        templateUrl: '/angularPartials/realTimeStream',
        link: function (scope, element, attrs) {
            scope.bookRequests = [];
            mySocket.on('book request', function (data) {

                scope.bookRequests.push(data);
            });
        }
    };
}]);
app.directive('shortListBook', ['service', function (service) {
    return {
        restrict: 'EA',
        scope: {
            "toUser": '=',
            "bookId": '=',
            "index": '=',
            "books": '='
        },
        link: function (scope, element, attrs) {
            element.bind('click', function () {

                service.executePost(service.withBaseUrl('/shortListBook'), {
                    toUser: scope.toUser,
                    bookId: scope.bookId
                }, function (data) {
                    scope.books[scope.index] = data.data.book;
                });
            });
        }
    };
}]);
app.directive('notificationDetail', ['service', '$uibModal', function (service, $uibModal) {
    return {
        restrict: 'A',
        scope: {
            "notificationId": '@',
            "messageId": '@',
            "bookId": '@',
        },
        link: function (scope, element, attrs) {
            element.bind('click', function () {


                var param = {
                    notificationId:scope.notificationId,
                    messageId:scope.messageId,
                    bookId:scope.bookId
                };

                var open = function (size, title, message, toUser, bookId) {
                    return $uibModal.open({
                        controller: 'notificationModalCtrl',
                        controllerAs: 'vm',
                        templateUrl: 'angularPartials/notificationDetail.jade',
                        size: size,
                        resolve: {
                            detail: function () {
                               return service.executePost(service.withBaseUrl('/notificationDetail'), param, function (data) {
                                    if (200 == data.status) {
                                        return data.data
                                    }
                                });
                            }
                        }
                    });
                };

                open();

            });
        }
    };
}]);

app.filter('reverse', function () {
    return function (items) {
        return items.slice().reverse();
    };
});
app.directive('modalTrigger', modalTriggerDirective)
app.directive('sidebarTerms', sidebarTerms)
app.directive('searchResult', ['service', '$stateParams', 'growl', '$rootScope', searchResult])
app.directive('scrollBody', ['service', '$window', scrollBody])
app.directive('subscribeTerms', ['service', '$rootScope', '$window', subscribeTerms]);
app.controller('bodyCtrl',['$scope','service',function(_,service){

}]);

app.controller('landingCtrl', ['$scope', '$stateParams', 'service', 'mySocket', function (_, $stateParams, service, mySocket) {


    //mySocket.on('book request', function () {
    //    console.log('book added');
    //});

}]);


app.controller('profileCtrl', ['$scope', 'mySocket', 'growl', function (_, mySocket, growl) {


}]);

app.controller('notificationModalCtrl', ['$scope', 'growl','detail','$uibModalInstance', function (_, growl,detail,$uibModalInstance) {

    var vm = this;
        vm.detail = detail;

    switch(detail.message.verb) {
        case "MESSAGE":
            vm.title="Message";
            break;
        case "LIKE":
            vm.title="Liked";
            break;
        default:

    }
    vm.cancel = $uibModalInstance.dismiss;

}]);

app.controller('realTimeStreamCtrl', ['$scope', 'mySocket', function (_, mySocket) {
    _.bookRequests = [];
}]);

app.controller('MyModalController', MyModalController)

function MyModalController($uibModalInstance, items, service) {
    console.log('modal controller');
    var vm = this;
    vm.content = items;
    console.log(items);
    vm.confirm = function () {
        service.executePost(service.withBaseUrl('/message'), {
            toUser: items.toUser,
            message: vm.message,
            bookId: items.bookId
        }, function (data) {
            vm.cancel();
        })
    };
    vm.cancel = $uibModalInstance.dismiss;
};

app.controller('loginCtrl', ['$scope', 'service', '$window', 'Auth', 'mySocket', function (_, service, $window, Auth, mySocket) {

    _.data = {};

    _.signIn = function () {

        service.executePost('/login', _.data, function (data) {
            //growl.success(data.data.msg)
            if (200 == data.status) {
                //fetching user object
                service.executeGet(service.withBaseUrl('/isLoggedIn'), {}, function (data) {

                    mySocket.emit('join', {email: data.data.api.local.email,uid: data.data.api._id});

                    Auth.set(data.data.api);

                    if (data.data.api.local.role == "admin") {
                        var url = "http://" + $window.location.host + "/admin#/admin/profile";
                    } else {
                        //TODO-bob Redirect user on first login to user edit form, it encourage user to fill more fields
                        var url = "http://" + $window.location.host + "/#/user/profile";
                    }

                    $window.location.href = url;
                });


            }
        });
    }

}]);

app.controller('bodyCtrl', ['$scope', 'service', '$rootScope', '$state', '$window', 'mySocket', 'growl', function (_, service, $rootScope, $state, $window, mySocket, growl) {

    init();

    mySocket.on("new_msg", function (data) {
        console.log('particular message', data.msg);
    });
    mySocket.on("bookShortListed", function (data) {
        console.log('bookShortListed message', data.msg);
        growl.success(data.msg);
    });
    mySocket.on("message_received", function (data) {
        console.log('bookShortListed message', data.msg);
        growl.success(data.msg);
    });
    mySocket.on("unreadNotification", function (data) {
        //console.log('unread', data.count);
        $rootScope.unreadNotifications = data.count;
    });

    _.$on('$destroy', function () {
        mySocket.removeListener();
    });


    function init() {
        service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
            $rootScope.streamTerms = data.data.terms;
        });
    }

    _.isOnLogin = function () {
        return $state.includes('login') || $state.includes('signup');
    }


}]);

app.controller('filterCtrl', ['$scope', 'terms', function (_, terms) {
    _.terms = terms;
}]);
app.controller('searchCtrl', ['$scope', '$stateParams', function (_, $stateParams) {

}]);

app.controller('verifyCtrl', ['$scope', 'verify', function (_, verify) {
    console.log(verify);
}]);
app.controller('requestBookCtrl', ['$scope', 'terms', 'service', 'growl', function (_, terms, service, growl) {
    _.terms = terms;
    _.data = {};

    //socket.on('book request', function (data) {
    //    console.log('my book request',data);
    //growl.info(data);
    //});

    _.requestBook = function () {
        service.executePost(service.withBaseUrl('/book/request'), _.data);
    }
}]);

app.controller('headerCtrl', ['$scope', '$rootScope', 'service', '$state', function (_, $rootScope, service, $state) {
    _.notifications = [];

    _.toggled = function (open) {
        if(open){
            service.executePost(service.withBaseUrl('/getNotifications'), _.data, function (data) {
                if (200 == data.status) {
                    //console.log(data.data.notification);
                    _.notifications = data.data.notification
                }
            });
        }

    }
    _.search = function () {
        $state.go("searchResult", {param: _.searchParam});

        //var query = {};
        //query.title = _.searchParam;
        //
        //service.executeGet(service.withBaseUrl('/search'),query,function(data){
        //    _.terms =  data.data.terms;
        //});
    }

}]);


app.controller('signUpCtrl', ['$scope', 'service', 'growl', '$window', '$state', 'vcRecaptchaService', function (_, service, growl, $window, $state, vcRecaptchaService) {


    service.executeGet(service.withBaseUrl('/fetchStreams'), {}, function (data) {
        _.terms = data.data.terms;
    });

    _.options = {
        types: '(regions)',
        country: 'in'
    }

    _.errorInEmail = function (createUser) {
        return !createUser.email.$valid && createUser.email.$dirty
    }

    _.openToolip = function (valid) {
        if (valid == undefined) {
            return false;
        } else {
            valid;
        }

    }


    function checkPwd(str) {
        if (str.length < 6) {
            return ("too_short");
        } else if (str.length > 50) {
            return ("too_long");
        } else if (str.search(/\d/) == -1) {
            return ("no_num");
        } else if (str.search(/[a-zA-Z]/) == -1) {
            return ("no_letter");
        } else if (str.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {
            return ("bad_char");
        }
        return "string password";
    }


    _.patternMessage = function (valid) {
        //console.log(valie);
        if (!valid) {
            return 'Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Characte';
        } else {
            return 'Strong Password';
        }
    }


    _.signUp = function () {


        _.data.latlng = {
            lat: _.details.geometry.location.lat(),
            lang: _.details.geometry.location.lng()
        }
        service.executePost('/signup', _.data, function (data) {
            if (200 == data.status) {
                growl.success('Verification email is sent to your email.');
                $state.go('logout');
            }
        });
    }

}]);

app.controller('postBookCtrl', ['$scope', 'terms', 'FileUploader', 'service', 'growl', function (_, terms, FileUploader, service, growl) {
    _.data = {};
    _.terms = terms;
    _.uploader = new FileUploader({
        alias: 'image',
        url: service.withBaseUrl('/upload/image'),
        formData: [1, 2, 3],
        onSuccessItem: function (item, response, status, headers) {

            _.data.img_info = response;

            service.executePost(service.withBaseUrl('/book/create'), _.data);
        },
        withCredentials: true
    });

    _.postBook = function () {
        _.uploader.uploadAll();
    }


}]);
var service = angular.module('ServiceModule', []);
service.factory('service', ['$http',  function ($http) {
    var factory = {};
    var urlBase = '/api';

    factory.executeGet = function (url, data, success, failure) {

        return $http({
            method: 'GET',
            url: url,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json"
            },
            params: data,
            withCredentials: true
        }).then(success,
            failure);

    };

    factory.withBaseUrl = function (url) {
        return urlBase + url;
    }

    factory.executePost = function (url, data, success, failure) {

        return $http({
            method: 'POST',
            url: url,
            dataType: 'json',
            headers: {
                "Content-Type": "application/json"
            },
            data: data,
            withCredentials: true
        }).then(success,
            failure);

    };

    return factory;
}]);

service.factory('authInterceptorService', ['$q','$location','$window','Auth','growl', function ($q, $location,$window,Auth,growl){
    var responseError = function (rejection) {
        if (rejection.status === 403 ||  rejection.status === 401) {
            growl.info('<strong>You need to login to access this page</strong>');
        }
        if (rejection.status === 400) {
            if(angular.isDefined(rejection.data.msg)){
                growl.error(rejection.data.msg);
            }
        }
        return $q.reject(rejection);
    };
    var response = function (response) {

        if (response.status === 200) {
            if(angular.isDefined(response.data.msg)){
                growl.success(response.data.msg);
            }
        }
        return response || $q.when(response);
    };

    return {
        responseError: responseError,
        response:response
    };
}]);

service.factory('Auth', ['$cookieStore', function ($cookieStore) {

    var _user = {};

    return {

        user : _user,

        set: function (_user) {
            // you can retrive a user setted from another page, like login sucessful page.
            existing_cookie_user = $cookieStore.get('current.user');
            _user =  _user || existing_cookie_user;

            $cookieStore.put('current.user', _user);
        },
        get: function (_user) {
            // you can retrive a user setted from another page, like login sucessful page.
            return $cookieStore.get('current.user');

        },

        remove: function () {
            console.log('removing cookie');
            $cookieStore.remove('current.user', _user);
        }
    };
}]);

service.factory('mySocket',['socketFactory','$location' ,function (socketFactory,$location) {
    return socketFactory({
        ioSocket: io.connect($location.protocol() + "://" + $location.host()+':8080')
    });
}]);

function myModalFactory($uibModal) {
    var open = function (size, title, message,toUser,bookId) {
        return $uibModal.open({
            controller: 'MyModalController',
            controllerAs: 'vm',
            templateUrl : 'angularPartials/message.jade',
            size: size,
            resolve: {
                items: function() {
                    return {
                        title: title,
                        message: message,
                        toUser:toUser,
                        bookId:bookId
                    };
                }
            }
        });
    };

    return {
        open: open
    };
}

app.factory('$myModal', myModalFactory)